<?php
/**
 * @framework 3wymiar.pl 3.0 2002-2012 (c)
 *
 * @author Marcin Cichy <cichy@3wymiar.pl>
 * @version 1.0
 */

class Upload
{
    function __construct()
    {
        $this->upload_dir; // katalog docelowy liczony od głównego bez / na końcu
        $this->upload_file_name = uniqid(); // nazwa pliku
        $this->upload_file_extension; // roszerzenie pliku
        $this->scale->make; // czy skalować
        $this->scale->type; // typ skalowania (kwadrat, prostokąt)
        $this->scale->size; // ile skala kwadrat
        $this->scale->width; // ile skala szerokość
        $this->scale->height; // ile skala wysokość
        $this->thumbnail->make; // czy robić miniaturkę
        $this->thumbnail->type; // typ skalowania (kwadrat, prostokąt)
        $this->thumbnail->size; // rozmiar miniaturki kwadrat
        $this->thumbnail->width; // ile miniaturka szerokość
        $this->thumbnail->height; // ile miniaturka wysokość
    }

    function copy_file($tmp)
    {
        if (empty($tmp)) {
            $info['error'] = 'brak pliku';
            return $info;
        } elseif (empty($this->upload_dir)) {
            $info['error'] = 'nie wybrano katalogu docelowego';
            return $info;
        } elseif (empty($this->upload_file_name) || empty($this->upload_file_extension)) {
            $info['error'] = 'brak nazwy pliku docelowego lub rozszerzenia';
            return $info;
        }

        move_uploaded_file($tmp, dirname(__FILE__) . '/../' . $this->upload_dir . '/' . $this->upload_file_name . '.' . $this->upload_file_extension);
        $info['ok'] = 'poprawny upload pliku';
        $info['file'] = $this->upload_dir . '/' . $this->upload_file_name . '.' . $this->upload_file_extension;
        return $info;
    }

    function img_upload($tmp)
    {
        if (empty($tmp)) {
            $info['error'] = 'brak pliku';
            return $info;
        } elseif (empty($this->upload_dir)) {
            $info['error'] = 'nie wybrano katalogu docelowego';
            return $info;
        } elseif ($size[2] > 3) {
            $info['error'] = 'nieprawidłowy format pliku, wymagany jpg, png lub gif';
            return $info;
        }

        $size = getimagesize($tmp);

        /* miniaturka */
        if ($this->thumbnail->make == true) {
            if ($this->thumbnail->type == 1) {
                $new_im = $this->make_square($tmp, $this->thumbnail->size);
            } elseif ($this->thumbnail->type == 2) {
                $new_im = $this->make_scaled($tmp, $this->thumbnail->width, $this->thumbnail->height);
            }

            if ($size[2] == 1) {
                $endfile = $this->upload_file_name . '_thumb.gif';
                imagegif($new_im, dirname(__FILE__) . '/../' . $this->upload_dir . '/' . $endfile);
            } elseif ($size[2] == 2) {
                $endfile = $this->upload_file_name . '_thumb.jpg';
                imagejpeg($new_im, dirname(__FILE__) . '/../' . $this->upload_dir . '/' . $endfile, 75);
            } elseif ($size[2] == 3) {
                $endfile = $this->upload_file_name . '_thumb.png';
                imagepng($new_im, dirname(__FILE__) . '/../' . $this->upload_dir . '/' . $endfile);
            }

            imagedestroy($new_im);
            chmod(dirname(__FILE__) . '/../' . $this->upload_dir . '/' . $endfile, 0777);
            $info['thumbnail'] = $this->upload_dir . '/' . $endfile;
        }

        /* skala */
        if ($this->scale->make == true) {

            if ($this->scale->type == 1) {
                $new_im = $this->make_square($tmp, $this->scale->size);
            } elseif ($this->scale->type == 2) {
                $new_im = $this->make_scaled($tmp, $this->scale->width, $this->scale->height);
            }

            if ($size[2] == 1) {
                $endfile = $this->upload_file_name . '.gif';
                imagegif($new_im, dirname(__FILE__) . '/../' . $this->upload_dir . '/' . $endfile);
            } elseif ($size[2] == 2) {
                $endfile = $this->upload_file_name . '.jpg';
                imagejpeg($new_im, dirname(__FILE__) . '/../' . $this->upload_dir . '/' . $endfile, 75);
            } elseif ($size[2] == 3) {
                $endfile = $this->upload_file_name . '.png';
                imagepng($new_im, dirname(__FILE__) . '/../' . $this->upload_dir . '/' . $endfile);
            }

            imagedestroy($new_im);
            chmod(dirname(__FILE__) . '/../' . $this->upload_dir . '/' . $endfile, 0777);
            $info['scale'] = $this->upload_dir . '/' . $endfile;
        }

        $info['ok'] = 'poprawny upload pliku';
        return $info;
    }

    function make_square($file, $max_size)
    {
        $size = getimagesize($file);
        if ($size[2] == 1) {
            $im = imagecreatefromgif($file);
        } elseif ($size[2] == 2) {
            $im = imagecreatefromjpeg($file);
        } elseif ($size[2] == 3) {
            $im = imagecreatefrompng($file);
        }

        $width = $size[0];
        $height = $size[1];
        if ($width >= $height) {
            $x = ceil(($width - $height) / 2);
            $width = $height;
        } elseif ($height > $width) {
            $y = ceil(($height - $width) / 2);
            $height = $width;
        }
        $new_im = imagecreatetruecolor($max_size, $max_size);
        imagecopyresampled($new_im, $im, 0, 0, $x, $y, $max_size, $max_size, $width, $height);
        imagedestroy($im);
        return $new_im;
    }

    function make_scaled($file, $max_width, $max_height)
    {
        $size = getimagesize($file);
        if ($size[2] == 1) {
            $im = imagecreatefromgif($file);
        } elseif ($size[2] == 2) {
            $im = imagecreatefromjpeg($file);
        } elseif ($size[2] == 3) {
            $im = imagecreatefrompng($file);
        }

        if ($size[0] >= $size[1]) {
            $ratio = round($max_width / $size[0], 6);
            $x = $max_width;
            $y = round($ratio * $size[1]);
        } else {
            $ratio = round($max_height / $size[1], 6);
            $y = $max_height;
            $x = round($ratio * $size[0]);
        }

        $endfile = $this->upload_file_name . '.jpg';
        $new_im = imagecreatetruecolor($x, $y);

        imagecopyresampled($new_im, $im, 0, 0, 0, 0, $x, $y, imagesx($im), imagesy($im));
        imagedestroy($im);
        return $new_im;
    }

    function filesize($path)
    {
        $bytes = sprintf('%u', filesize($path));

        if ($bytes > 0) {
            $unit = intval(log($bytes, 1024));
            $units = array('B', 'KB', 'MB', 'GB');

            if (array_key_exists($unit, $units) === true) {
                return sprintf('%d %s', $bytes / pow(1024, $unit), $units[$unit]);
            }
        }

        return $bytes;
    }
}

?>