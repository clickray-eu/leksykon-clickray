<?php
/**
 * @framework 3wymiar.pl 3.0 2002-2013 (c)
 *
 * @author Marcin Cichy <cichy@3wymiar.pl>
 * @version 1.0
 */

class Site_builder
{
    function __construct($from = false)
    {
        /*
          * zmienne sekcji <head> wykorzystywane w templatach
          * zawartość można modyfikować w dowolnym momencie
         */

        /* zmienne przekazujące czy jesteśmy w katalogu subdomen */
        $this->from = ($from != false) ? '../../' : '';
        $this->from_dir = ($from != false) ? $from . '/' : '';

        /* zmienne budowania serwisu */
        $this->site_title = array();
        $this->site_description = '';
        $this->site_keywords = '';
        $this->css_list = array();
        $this->js_list = array();
        /*
         * zmienne wykorzystywane przy budowie html
        */
        $this->loaded_site = '';
    }

    /*
      * zwracamy zebraną stronę w postaci html
      */
    function content_html($loading_site)
    {
        /*
          * jeżeli nie mamy zdefiniowanej podstrony wgrywamy główną dla danego serwisu
          */
        if (empty($loading_site)) {
            $loading_site = $this->service_config->main_page;
        }
        $this->loaded_site = $loading_site;

        /*
         * wgrywamy główny template
        */
        $template = new Template($this->service_config->template_file, '');
        return $template->content_html();
    }

    /*
     * generujemy tytuł strony
      * doklejamy do globalnego tytułu zmiany określane w trakcie generowania strony
      * wszystko rozdzielamy globalanymi separatorami
      */
    function get_site_title()
    {
        krsort($this->site_title);
        $title = '';
        if (count($this->site_title) > 0) {
            foreach ($this->site_title AS $t) {
                $title .= $t . $this->service_config->global_separator;
            }
        }
        $title .= $this->service_config->global_title;
        return $title;
    }

    /*
      * wgrywa dany moduł i zwraca go w postaci html
      */

    function load_module($module_name)
    {
        global $sql;
        $this->js_list[] = $module_name;
        $this->css_list[] = $module_name;
        /*
          * wgrywa kontroler modułu - jeżeli istnieje
          * wgrywa widok pliku
          * jeżeli nie ma widoku, zwraca error
         */
        $html = '';
        if (file_exists(dirname(__FILE__) . '/../' . $this->from_dir . 'modules/' . $module_name . '/view.php')) {
            if (file_exists(dirname(__FILE__) . '/../' . $this->from_dir . 'modules/' . $module_name . '/controller.php')) {
                require_once(dirname(__FILE__) . '/../' . $this->from_dir . 'modules/' . $module_name . '/controller.php');
            }
            /*
             * jeżeli chcemy nadpisać widok, musimy to zrobić w kontrolerze
             * przekazując nazwę innego pliku, np.:
             * $show_view = 'lato'; dla moduł/lato.php
            */
            $view = (!empty($show_view)) ? $show_view : 'view';
            ob_start();
            require_once(dirname(__FILE__) . '/../' . $this->from_dir . 'modules/' . $module_name . '/' . $view . '.php');
            return ob_get_clean();
        } else {
            $this->error_redirect('brak modułu ' . $module_name);
            exit();
        }
    }

    /*
      * wgrywa daną podstronę i zwraca ją w postaci html
      */
    function load_site()
    {
        global $sql;
        $this->css_list[] = $this->loaded_site;
        $this->js_list[] = $this->loaded_site;
        $config = new Config();

        if ($sql->get_var("
                SELECT COUNT(id)
                FROM 3w_components
                WHERE url = '" . mysqli_escape_string(mysqli_connect($config->get_sql_config()->host, $config->get_sql_config()->user, $config->get_sql_config()->password, $config->get_sql_config()->database), $this->loaded_site) . "' 
                AND deleted_at = '0'") == 1) {

            /*
              * wgrywamy dane strony
              * wgrywamy controller i widok (klucz type_name)
             */

            $component = $sql->get_row("
		        SELECT id, parent_id, url, site_title, site_description, site_keywords, file, body
		        FROM 3w_components
		        WHERE deleted_at = '0' AND url = '" . mysqli_escape_string(mysqli_connect($config->get_sql_config()->host, $config->get_sql_config()->user, $config->get_sql_config()->password, $config->get_sql_config()->database), $this->loaded_site) . "'
		        ");

            $this->loaded_component = $component;

            /*
             * wgrywamy parent strony
            */
            if ($component->parent_id > 0) {
                $this->loaded_component->parent = $sql->get_row("
		            SELECT id, url, site_title
		            FROM 3w_components
		            WHERE deleted_at = '0' AND id = '" . intval($component->parent_id) . "'
		            ");

                $this->site_title[] = stripslashes($this->loaded_component->parent->site_title);
            }
            $this->site_title[] = stripslashes($this->loaded_component->site_title);

            /*
             * wgrywamy controller i plik widoku komponentu dedykowanego
            */
            if (empty($component->file)) {
                return stripslashes($component->body);
            } else {
                if (file_exists(dirname(__FILE__) . '/../' . $this->from_dir . 'components/' . $component->file . '/view.php')) {
                    if (file_exists(dirname(__FILE__) . '/../' . $this->from_dir . 'components/' . $component->file . '/controller.php')) {
                        require_once(dirname(__FILE__) . '/../' . $this->from_dir . 'components/' . $component->file . '/controller.php');
                    }
                    $view = (!empty($show_view)) ? $show_view : 'view';
                    ob_start();
                    require_once(dirname(__FILE__) . '/../' . $this->from_dir . 'components/' . $component->file . '/' . $view . '.php');
                    return ob_get_clean();
                }
            }
        } /*
	  * wgrywamy controller i plik widoku komponentu dedykowanego
	 */
        elseif (file_exists(dirname(__FILE__) . '/../' . $this->from_dir . 'components/' . $this->loaded_site . '/view.php')) {
            if (file_exists(dirname(__FILE__) . '/../' . $this->from_dir . 'components/' . $this->loaded_site . '/controller.php')) {
                require_once(dirname(__FILE__) . '/../' . $this->from_dir . 'components/' . $this->loaded_site . '/controller.php');
            }
            $view = (!empty($show_view)) ? $show_view : 'view';
            ob_start();
            require_once(dirname(__FILE__) . '/../' . $this->from_dir . 'components/' . $this->loaded_site . '/' . $view . '.php');
            return ob_get_clean();
        } else {
            $this->error_redirect('brak podstrony ' . $this->loaded_site);
            exit();
        }
    }

    /*
     * robi listę plików css wgrywanych dla danej podstrony
      * listę plików można zmienić w dowolnym momencie poprzez nadanie
      * $site->css_list[] = 'nazwa_pliku';
      * jeżeli chcemy dodać kilka różnych plików możemy użyć:
      * $site->css_list = array_merge($this->css_list, array('plik1','plik2','plik3'));
      */
    function make_css_list()
    {
        $list = '';
        $this->server_config->public_dir->host = "10.0.0.92/leksykon-clickray/";
        foreach (array_unique($this->css_list) AS $l) {
            if (file_exists(dirname(__FILE__) . '/../' . $this->from_dir . $this->server_config->public_dir->css . $l . '.css')) {
                $list .= '<link rel="stylesheet" href="http://' . $this->server_config->public_dir->host . '/' . $this->from_dir . $this->server_config->public_dir->css . $l . '.css" type="text/css" />';
            } elseif (file_exists(dirname(__FILE__) . '/../' . $this->server_config->public_dir->css . $l . '.css')) {
                $list .= '<link rel="stylesheet" href="http://' . $this->server_config->public_dir->host . '/' . $this->server_config->public_dir->css . $l . '.css" type="text/css" />';
            }
        }
        return $list;
    }

    /*
     * robi listę plików js wgrywanych dla danej podstrony
      * listę plików można zmienić w dowolnym momencie poprzez nadanie
      * $site->js_list[] = 'nazwa_pliku';
      * jeżeli chcemy dodać kilka różnych plików możemy użyć:
      * $site->js_list = array_merge($this->js_list, array('plik1','plik2','plik3'));
      */
    function make_js_list()
    {
        $list = '';
        $this->server_config->public_dir->host = "10.0.0.92/leksykon-clickray";
        foreach (array_unique($this->js_list) AS $l) {
            if (file_exists(dirname(__FILE__) . '/../' . $this->from_dir . $this->server_config->public_dir->js . $l . '.js')) {
                $list .= '<script type="text/javascript" src="http://' . $this->server_config->public_dir->host . '/' . $this->from_dir . $this->server_config->public_dir->js . $l . '.js"></script>';
            } elseif (file_exists(dirname(__FILE__) . '/../' . $this->server_config->public_dir->js . $l . '.js')) {
                $list .= '<script type="text/javascript" src="http://' . $this->server_config->public_dir->host . '/' . $this->server_config->public_dir->js . $l . '.js"></script>';
            }
        }
        return $list;
    }

    /*
     * parsujemy aktualny REQUEST_URI wywalając z niego określony parametr i jeżeli występuje nowy nadpisując go
      * $delete_param może być jako array albo string
      * $add_param w postaci array(nazwa => wartość)
      */
    function parse_request_uri($delete_param, $add_param)
    {
        $uri = explode('?', $_SERVER['REQUEST_URI']);
        $url = $uri[0];
        $params = explode('&', $uri[1]);
        $i = 0;
        $param_exists = false;

        foreach ($params AS $p) {
            $param = explode('=', $p);
            if ($param[1] != '' && $this->is_param_allowed($param[0], $delete_param) == true) {
                $i++;
                $prefix = ($i == 1) ? '?' : '&';
                $url .= $prefix . $param[0] . '=' . $param[1];
                $param_exists = true;
            }
        }

        foreach ($add_param AS $key => $val) {
            if ($param_exists == false) {
                $url .= '?' . $key . '=' . $val;
                $param_exists = true;
            } else {
                $url .= '&' . $key . '=' . $val;
            }
        }
        return $url;
    }

    /*
     * sprawdzanie czy dany parametr może występować w nowym URL
      */
    function is_param_allowed($param, $delete_param)
    {
        if (is_array($delete_param)) {
            foreach ($delete_param AS $dp) {
                if ($param == $dp) {
                    return false;
                }
            }
        } else {
            if ($param == $delete_param) {
                return false;
            }
        }
        return true;
    }

    /*
     * wywalamy niechciane znaki parserem
      */
    function parse_name_for_url($url)
    {
        $url = trim($url);
        $url = str_replace(array('Ą', 'Ć', 'Ę', 'Ł', 'Ń', 'Ó', 'Ś', 'Ż', 'Ź'), array('a', 'c', 'e', 'l', 'n', 'o', 's', 'z', 'z'), $url);
        $url = str_replace(array('ą', 'ć', 'ę', 'ł', 'ń', 'ó', 'ś', 'ż', '&#317;'), array('a', 'c', 'e', 'l', 'n', 'o', 's', 'z', 'z'), $url);
        $url = str_replace(array(' ', '.'), '_', $url);
        $url = str_replace(array('@', '#', '$', '%', '&', '!', ',', '/', '\\', '(', ')', '-', ':'), '', $url);
        return strtolower($url);
    }

    /* redirect errorów */
    function error_redirect($error_text)
    {
        $_SESSION['alert']->type = 'error';
        $_SESSION['alert']->text = $error_text;
        header('location: index.php');
        echo $error_text;
        exit();
    }
}

?>