<?php
/**
 * @framework 3wymiar.pl 3.0 2002-2013 (c)
 *
 * @author Marcin Cichy <cichy@3wymiar.pl>
 * @version 1.0
 */

class Auth
{
    function __construct()
    {

    }

    /*
     * autoryzacja admina
      */
    function authenticate_admin($username, $password)
    {
        global $sql;
        global $translate;
        $config = new Config();
        $user = $sql->get_row("SELECT id FROM 3w_users WHERE uid = '" . mysqli_escape_string(mysqli_connect($config->get_sql_config()->host, $config->get_sql_config()->user, $config->get_sql_config()->password, $config->get_sql_config()->database), $username) . "' AND password = password('" . mysqli_escape_string(mysqli_connect($config->get_sql_config()->host, $config->get_sql_config()->user, $config->get_sql_config()->password, $config->get_sql_config()->database), $password) . "') AND privileges_id = '1' AND blockade = '0' AND deleted_at = '0'");
        if (intval($user->id) > 0) {
            $this->load_user($user->id);
            $_SESSION['alert']->type = 'success';
            $_SESSION['alert']->text = 'Zalogowowano poprawnie.';
            header('location: home.html');
            exit();
        } else {
            unset($_SESSION['user']);
            $_SESSION['alert']->type = 'error';
            $_SESSION['alert']->text = 'Niepoprawna nazwa użytkownika lub hasło';
            header('location: login.html');
            exit();
        }
    }

    /*
     * autoryzacja nieaktywnego użytkownika
      */
    function authenticate_user($username, $password)
    {
        global $sql;
        global $translate;
        $config = new Config();
        $user = $sql->get_row("SELECT id FROM 3w_users WHERE uid = '" . mysqli_escape_string(mysqli_connect($config->get_sql_config()->host, $config->get_sql_config()->user, $config->get_sql_config()->password, $config->get_sql_config()->database), $username) . "' AND password = password('" . mysqli_escape_string(mysqli_connect($config->get_sql_config()->host, $config->get_sql_config()->user, $config->get_sql_config()->password, $config->get_sql_config()->database), $password) . "')  AND privileges_id != '1' AND blockade = '0' AND deleted_at = '0'");
        if (intval($user->id) > 0) {
            $this->load_user($user->id);
            $_SESSION['alert']->type = 'success';
            $_SESSION['alert']->text = 'Zalogowowano poprawnie.';
            header('location: home.html');
            exit();
        } else {
            unset($_SESSION['user']);
            $_SESSION['alert']->type = 'error';
            $_SESSION['alert']->text = 'Niepoprawna nazwa użytkownika lub hasło';
            header('location: login.html');
            exit();
        }
    }

    /*
     * wgrywanie zalogowanego usera
      */
    private function load_user($id)
    {
        global $sql;

        $user = $sql->get_row("SELECT * FROM 3w_users WHERE id = '" . intval($id) . "'");
        $user_privileges = $sql->get_row("SELECT * FROM 3w_user_privileges WHERE id = '" . intval($user->privileges_id) . "'");

        $user->admin = $user_privileges->admin;
        $user->privilege_name = $user_privileges->privilege_name;
        $user->privileges = (!empty($user->privileges)) ? $user->privileges : $user_privileges->privileges;
        $user->homepage = 'home';
        $user->name = $user->first_name . ' ' . $user->last_name;
        unset($user->password);

        $_SESSION['user'] = $user;
        return true;
    }
}

?>