<?php
/**
 * @framework 3wymiar.pl 3.0 2002-2013 (c)
 *
 * @author Marcin Cichy <cichy@3wymiar.pl>
 * @version 1.0
 */

class Form
{
    function __construct()
    {
        global $site;
        $site->css_list[] = 'form';
        $site->js_list[] = 'form';
    }

    function parse_query($hub_query)
    {
        global $sql;
        $exp = explode(':', str_replace(array('[', ']'), '', $hub_query));
        $table = explode(',', $exp[0]);
        $array = explode('=', $exp[1]);
        $join = explode('=', $exp[2]);
        if (!empty($join[0])) {
            return $sql->get_results("SELECT " . $array[0] . " AS id, " . $array[1] . " AS value FROM " . $table[0] . " LEFT JOIN " . $table[1] . " ON " . $join[0] . " = " . $join[1] . " WHERE " . $table[0] . ".deleted_at = '0' AND " . $table[1] . ".deleted_at = '0' ORDER BY " . $array[1] . " ASC");
        } else {
            return $sql->get_results("SELECT " . $array[0] . " AS id, " . $array[1] . " AS value FROM " . $table[0] . " WHERE " . $table[0] . ".deleted_at = '0' ORDER BY `value` ASC");
        }
    }

    function numberToRoman($num)
    {
        $n = intval($num);
        $result = '';
        $lookup = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400,
            'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40,
            'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);

        foreach ($lookup as $roman => $value) {
            $matches = intval($n / $value);
            $result .= str_repeat($roman, $matches);
            $n = $n % $value;
        }
        return $result;
    }

    function numberToLetter($num)
    {
        $letters = ' a b c d e f g h i j k l m n o p r s t u w x y z';
        $letter = explode(' ', $letters);
        return $letter[$num];
    }

    function letterToNumber($letter)
    {
        $letters = ' a b c d e f g h i j k l m n o p r s t u w x y z';
        $exp = explode(' ', $letters);
        $i = 0;
        foreach ($exp AS $e) {
            if ($e == $letter) {
                return $i;
            }
            $i++;
        }
    }

    function ans_int($ans, $type, $list)
    {
        if ($type == 'lower-alpha') {
            return $this->letterToNumber($ans);
        } elseif ($type == 'upper-alpha') {
            return $this->letterToNumber(strtolower($ans));
        } elseif ($type == 'decimal-leading-zero') {
            if ($ans < 10) {
                $ans = '0' . $ans;
            }
            return $ans;
        } elseif ($type == 'upper-roman') {
            return $ans;
        } elseif ($type == 'own') {
            foreach ($list AS $key => $val) {
                if ($val == $ans) {
                    return $key + 1;
                }
            }
        } else {
            return $ans;
        }
    }
}

/*
 * input tekstowy
*/

class Input extends Form
{
    function __construct()
    {
        /*
          * zmienne używane w polu
         */
        $this->required = ''; // czy wymagane
        $this->maxlength = ''; // maksymalna długość
        $this->add_class = ''; // dodatkowe klasy
        $this->name = ''; // nazwa pola
        $this->field_id = ''; // wartość taga id=""
        $this->value = ''; // wartość pola
        $this->width = ''; // szerokość pola w px
        $this->placeholder = '';
        $this->style = ''; // styl inputa
        $this->disabled = '';
        $this->autocomplete = 1;
    }

    /*
     * generujemy i zwracamy input
      */
    function show()
    {
        $id = (!empty($this->field_id)) ? 'id="' . $this->field_id . '"' : '';
        $class = ' form_input ';
        $class .= ($this->required == 1 || $this->required == 'on') ? ' required_field ' : '';
        $class .= $this->add_class;
        $placeholder = (!empty($this->placeholder)) ? 'placeholder="' . $this->placeholder . '"' : '';
        $width = ($this->width > 0) ? 'width:' . $this->width . 'px' : '';
        $maxlenght = ($this->maxlength > 0) ? 'maxlength="' . $this->maxlength . '"' : '';
        $value = (!empty($this->value)) ? stripslashes($this->value) : '';
        $disabled = (!empty($this->disabled)) ? 'disabled="disabled"' : '';
        $autocomplete = ($this->autocomplete != 1) ? 'autocomplete="off"' : '';
        $html = '<input type="text" name="' . $this->name . '" ' . $id . ' ' . $placeholder . ' ' . $disabled . ' class="' . $class . '" ' . $maxlenght . ' value="' . $value . '" ' . $autocomplete . ' style="' . $this->style . ';' . $width . '" />';
        return $html;
    }
}

/*
 * pole textarea
*/

class Textarea extends Form
{
    function __construct()
    {
        /*
          * zmienne używane w polu
         */
        $this->required = ''; // czy wymagane
        $this->add_class = ''; // dodatkowe klasy
        $this->name = ''; // nazwa pola
        $this->field_id = ''; // wartość taga id=""
        $this->value = ''; // wartość pola
        $this->width = ''; // szerokość pola
        $this->height = ''; // wyokość pola
        $this->show_wysiwyg = ''; // pokaż WYSIWYG
        $this->user_wysiwyg = ''; // prosty wyswig
        $this->allow_html = ''; // zezwól na html i pokaż ikonę przełącznika

        global $site;
        /*$site->js_list = array_merge($site->js_list, array('ui/minified/jquery.ui.sortable.min','ui/minified/jquery.ui.mouse.min','ui/minified/jquery.ui.position.min','ui/minified/jquery.ui.widget.min','ui/i18n/jquery.ui.datepicker-pl','editor/jHtmlArea-0.7.0.min','editor'));
        $site->css_list = array_merge($site->css_list, array('editor/jHtmlArea','jHtmlArea.Editor'));*/
    }

    /*
     * generujemy i zwracamy input
      */
    function show()
    {

        $id = (!empty($this->field_id)) ? 'id="' . $this->field_id . '"' : '';
        $class = ' form_textarea ';
        $class .= ($this->required == 1 || $this->required == 'on') ? ' required_field ' : '';
        $class .= str_replace('dialogEditor', '', $this->add_class);
        if (SERVICE_NAME == 'admin') {
            $class .= ($this->show_wysiwyg == 1 || $this->show_wysiwyg == 'on') ? ' dialogAdminEditor  ' : '';
        } else {
            $class .= ($this->show_wysiwyg == 1 || $this->show_wysiwyg == 'on') ? ' dialogEditor  ' : '';
            $class .= ($this->user_wysiwyg == 1 || $this->show_wysiwyg == 'on') ? ' simpleDialogEditor  ' : '';
        }

        $width = ($this->width > 0) ? 'width:' . $this->width . 'px;' : '';
        $height = ($this->height > 0) ? 'height:' . $this->height . 'px;' : '';
        $style = ($this->width > 0 || $this->height > 0) ? 'style="' . $width . $height . '"' : '';

        $html = '<textarea name="' . $this->name . '" ' . $id . ' class="' . $class . '" ' . $style . '>' . stripslashes($this->value) . '</textarea>';
        return $html;
    }
}

/*
 * pole checkbox
*/

class Checkbox extends Form
{
    function __construct()
    {
        /*
          * zmienne używane w polu
         */
        $this->required = ''; // czy wymagane
        $this->add_class = ''; // dodatkowe klasy
        $this->name = ''; // nazwa pola
        $this->field_id = ''; // wartość taga id=""
        $this->value = ''; // wartość pola
        $this->checked = ''; // czy jest zaznaczony
        $this->disabled = ''; // blokuje element
    }

    /*
     * generujemy i zwracamy input
      */
    function show()
    {
        $id = (!empty($this->field_id)) ? 'id="' . $this->field_id . '"' : '';
        $class = ' form_checkbox ';
        $class .= ($this->required == 1 || $this->required == 'on') ? ' required_field ' : '';
        $class .= $this->add_class;
        $checked = ($this->checked == 1 || $this->checked == 'on') ? 'checked="checked"' : '';
        $value = (!empty($this->value)) ? stripslashes($this->value) : 1;
        $disabled = (!empty($this->disabled)) ? 'disabled="disabled"' : '';
        $html = '<input type="checkbox" name="' . $this->name . '" ' . $id . ' ' . $disabled . ' class="' . $class . '" value="' . $value . '" ' . $checked . ' />';
        return $html;
    }
}

/*
 * pole select z wyborem elementów
*/

class Select extends Form
{
    function __construct()
    {
        /*
          * zmienne używane w polu
         */
        $this->required = ''; // czy wymagane
        $this->add_class = ''; // dodatkowe klasy
        $this->name = ''; // nazwa pola
        $this->width = ''; // szerokość pola w px
        $this->field_id = ''; // wartość taga id=""
        $this->first_value = ''; // wartość pierwszego pola w postaci 'klucz'=>'wartość'
        $this->selected = ''; // wartość zaznaczonego klucza
        $this->elements = ''; // tablica w postaci 'klucz'=>'wartość' opcji selecta lub 'klucz'=>array(wartość,klasa,id,grupa)
        $this->disabled = ''; // blokuje element
        $this->readonly = ''; // blokuje element
    }

    /*
     * generujemy i zwracamy input
      */
    function show()
    {
        if (!is_array($this->elements) && substr($this->elements, 0, 1) == '[') {
            $this->elements = $this->parse_query($this->elements);
        } elseif (!is_array($this->elements) && strpos($this->elements, ',') !== false) {
            $exp = explode(',', $this->elements);
            $this->elements = $exp;
        }

        $id = (!empty($this->field_id)) ? 'id="' . $this->field_id . '"' : '';
        $class = ' form_select ';
        $class .= ($this->required == 1 || $this->required == 'on') ? ' required_field ' : '';
        $class .= $this->add_class;
        $disabled = (!empty($this->disabled)) ? 'disabled="disabled"' : '';
        $readonly = (!empty($this->readonly)) ? 'readonly="readonly"' : '';
        $width = ($this->width > 0) ? 'style="width:' . $this->width . 'px"' : '';
        $html = '<select name="' . $this->name . '" ' . $id . ' class="' . $class . '" ' . $disabled . ' ' . $readonly . ' ' . $width . '>';
        if (is_array($this->first_value)) {
            $html .= '<option value="' . key($this->first_value) . '">' . $this->first_value[key($this->first_value)] . '</option>';
        } elseif (!empty($this->first_value)) {
            $html .= '<option value="">' . $this->first_value . '</option>';
        }

        $el_group = 'group_name';
        $opened_group = false;
        foreach ($this->elements AS $key => $val) {
            if (is_array($val)) {
                $selected = ($this->selected == $key) ? 'selected="selected"' : '';
                $el_class = (!empty($val[1])) ? 'class="' . $val[1] . '"' : '';
                $el_id = (!empty($val[2])) ? 'id="' . $val[2] . '"' : '';
                if ($el_group != $val[3] && !empty($val[3])) {
                    if ($opened_group == true) {
                        $html .= '</optgroup>';
                    }
                    $html .= '<optgroup label="' . $val[3] . '">';
                    $el_group = $val[3];
                    $opened_group = true;
                }
                $html .= '<option value="' . $key . '" ' . $selected . ' ' . $el_id . ' ' . $el_class . '>' . $val[0] . '</option>';
            } elseif (is_object($val)) {
                $selected = ($this->selected == $val->id) ? 'selected="selected"' : '';
                $html .= '<option value="' . $val->id . '" ' . $selected . '>' . $val->value . '</option>';
            } else {
                $selected = ($this->selected == $key) ? 'selected="selected"' : '';
                $html .= '<option value="' . $key . '" ' . $selected . '>' . $val . '</option>';
            }
        }
        if ($opened_group == true) {
            $html .= '</optgroup>';
        }
        $html .= '</select>';
        return $html;
    }
}

/*
 * input szukaj w formie dialogu zamiast selecta
*/

class Dialog_select extends Form
{
    function __construct()
    {
        /*
          * zmienne używane w polu
         */
        $this->required = ''; // czy wymagane
        $this->add_class = ''; // dodatkowe klasy
        $this->name = ''; // nazwa pola
        $this->field_id = ''; // wartość taga id=""
        $this->first_value = ''; // wartość pierwszego pola w postaci 'klucz'=>'wartość'
        $this->selected = ''; // wartość zaznaczonego klucza
        $this->elements = ''; // tablica w postaci 'klucz'=>'wartość' opcji selecta lub 'klucz'=>array(wartość,klasa,id,grupa)
        $this->disabled = ''; // blokuje element
        $this->input = ''; // rodzaj inputa 'radio' lub 'chcekbox'
        $this->width = 300; // szerokość dialogu
        $this->height = 400; // wysokość dialogu
        $this->value_var = 'value'; // nazwa wartości odpowiedzialnej za wybór
        $this->table = ''; // tabela danych w postaci 'nagłówek' => 'wartość'
        $this->li_style = ''; // styl li
        $this->show_first = true; // czy pokazywać pierwszą wartość na liście
        $this->insert_value = false; // zarządzanie wartością danych poprzez przekazanie typu li
        $this->own_values = '';
    }

    /*
     * generujemy i zwracamy input
      */
    function show()
    {
        if (!is_array($this->elements) && substr($this->elements, 0, 1) == '[') {
            $this->elements = $this->parse_query($this->elements);
        } elseif (!is_array($this->elements) && strpos($this->elements, ',') !== false) {
            $exp = explode(',', $this->elements);
            $this->elements = $exp;
        }

        $this->field_id = (!empty($this->field_id)) ? $this->field_id : uniqid();
        $class = ' form_dialog_select ';
        $class .= ($this->required == 1 || $this->required == 'on') ? ' required_field ' : '';
        $class .= $this->add_class;
        $disabled = (!empty($this->disabled)) ? 'disabled="disabled"' : '';
        $value_var = $this->value_var;
        $html = '<script type="text/javascript">$(document).ready(function(){dialog_select_ready(\'' . $this->field_id . '\',{width:' . $this->width . ',height:' . $this->height . '});});</script>';
        /*
        <select name="'.$this->name.'" '.$id.' class="'.$class.'" '.$disabled.'>';*/
        if (!empty($this->selected)) {
            foreach ($this->elements AS $key => $var) {
                if ($this->selected == $var->id) {
                    $value = $var->$value_var;
                }
            }
        }
        /*elseif(!empty($this->first_value)){
            $html .= '<option value="">'.$this->first_value.'</option>';
          }
          /*foreach($this->elements AS $key => $val){
            $selected = ($this->selected == $val->id)?'selected="selected"':'';
            $html .= '<option value="'.$val->id.'" '.$selected.'>'.$val->value.'</option>';
          }*//*
	 $html .= '</select>*/
        $html .= '<input type="text" name="skip_this_' . $this->name . '" id="' . $this->field_id . '" class="' . $class . '" ' . $disabled . ' placeholder="' . $this->first_value . '" showonly="showonly" value="' . $value . '" />
		            <input type="hidden" name="' . $this->name . '"  id="' . $this->field_id . '_value" value="' . $this->selected . '" />';

        $html .= '<div id="' . $this->field_id . '_dialog" class="select_dialog" style="display:none">';
        if ($this->show_first != false) {
            $html .= '<ul class="nav nav-list"><li><a href="#" id="element_0" class="nolink">' . $this->first_value . '</a></li><li class="divider"></li></ul>';
        }
        $html .= '<ul class="nav nav-list">';
        $i = 0;
        foreach ($this->elements AS $key => $var) {
            $i++;
            if ($this->insert_value == false) {
                $value = $var->id;
            } else {
                if ($this->insert_value == 'none' || $this->insert_value == 'decimal') {
                    $value = $i;
                } elseif ($this->insert_value == 'decimal-leading-zero') {
                    $value = ($i < 10) ? '0' . $i : $i;
                } elseif ($this->insert_value == 'lower-alpha') {
                    $value = $this->numberToLetter($i);
                } elseif ($this->insert_value == 'upper-alpha') {
                    $value = strtoupper($this->numberToLetter($i));
                } elseif ($this->insert_value == 'upper-roman') {
                    $value = strtoupper($this->numberToRoman($i));
                } elseif ($this->insert_value == 'own') {
                    $value = $this->own_values[$i - 1];
                }
            }

            $selected = ($this->selected == $var->id) ? 'class="active"' : '';
            $icon = (!empty($var->icon)) ? '<i class="icon-' . $var->icon . '"></i> ' : '';
            $style = (!empty($this->li_style)) ? 'style="' . $this->li_style . '"' : '';
            $html .= '<li ' . $selected . ' ' . $style . '><a href="#" id="element_' . $value . '" class="nolink">' . $icon;
            /*		    if(!empty($this->table)){
                         }
                         else{*/
            $html .= $var->$value_var;
            //}
            $html .= '</a></li>';
        }
        $html .= '</ul>
	 </div>';
        return $html;
    }
}

/*
 * pole lista cheboxów
*/

class Checkboxs_list extends Form
{
    function __construct()
    {
        /*
          * zmienne używane w polu
         */
        $this->required = ''; // czy wymagane przynajmniej jedno
        $this->add_class = ''; // dodatkowe klasy
        $this->name = ''; // nazwa pola
        $this->checked = ''; // czy jest domyślnie zaznaczony
        $this->elements = ''; // tablica w postaci 'klucz'=>'wartość' opcji selecta lub 'klucz'=>array(wartość,klasa,id,grupa)
        $this->separator = ''; // separator pomiędzy elementami w postaci html
        $this->width = ''; // szerokość pola
        $this->checked_list = ''; // lista zaznaczonych
        $this->ul_class = ''; // classy dodawanie do ul
        $this->li_class = ''; // classy dodawanie do li
        $this->li_style = ''; // style dodawanie do li
        $this->insert_value = false; // zarządzanie wartością danych poprzez przekazanie typu li
        $this->own_values = ''; // lista własnych wartości
        $this->add_inputs = ''; // lista inputów do wyświetlenia
    }

    /*
     * generujemy i zwracamy inputy
      */
    function show()
    {
        if (!is_array($this->elements) && substr($this->elements, 0, 1) == '[') {
            $this->elements = $this->parse_query($this->elements);
        }
        $html = '';
        $i = 0;
        foreach ($this->elements AS $key => $var) {
            $i++;
            $id = (!empty($this->field_id)) ? 'id="' . $this->field_id . '_' . $var->id . '"' : '';
            $name = $this->name . '_' . $var->id;
            $class = ' form_list_checkbox ';
            $class .= ($this->required == 1 || $this->required == 'on') ? ' required_field ' : '';
            $class .= $this->add_class;
            $checked = '';
            foreach ($this->checked_list AS $chbox) {
                if ($chbox == $var->id) {
                    $checked = 'checked="checked"';
                }
            }

            if ($this->insert_value == false) {
                $value = 1;
            } else {
                $value = $i;
            }

            $style = ($this->separator == 'float') ? 'float:left;' : '';
            $style .= ($this->width > 0) ? 'width:' . $this->width . 'px;' : '';
            $style .= (!empty($this->li_style)) ? $this->li_style : '';
            $instyle = (!empty($style)) ? 'style="' . $style . '"' : '';

            $add_input = '';
            if (!empty($this->add_inputs[$i - 1])) {
                $placeholder = ($this->add_inputs[$i - 1] != 1) ? $this->add_inputs[$i - 1] : '';
                foreach ($this->add_inputs_values AS $sk => $sv) {
                    if ($sk == $this->field_id . '_' . $key . '_input') {
                        $sm_value = $sv;
                    }
                }
                $add_input = ' <input type="text" id="' . $this->field_id . '_' . $key . '_ainput" name="' . $name . '_input" class="check_parent" value="' . $sm_value . '" placeholder="' . $placeholder . '" /> ';
            }

            $html .= '<li ' . $instyle . ' class="' . $this->li_class . '"><input type="checkbox" name="' . $name . '" id="' . $this->field_id . '_' . $var->id . '" class="' . $class . '" value="' . $value . '" ' . $checked . ' /> <label for="' . $this->field_id . '_' . $var->id . '">' . stripslashes($var->value) . '</label>' . $add_input . '</li>';
        }
        return '<ul class="form_list_checkbox ' . $this->ul_class . '">' . $html . '</ul>';
    }
}


/*
 * wybór z listy radio buttonów rozdzielonych htmlem
*/

class Radio extends Form
{
    function __construct()
    {
        /*
          * zmienne używane w polu
         */
        $this->required = ''; // czy wymagane
        $this->add_class = ''; // dodatkowe klasy
        $this->name = ''; // nazwa pola
        $this->field_id = ''; // wartość taga id=""
        $this->selected = ''; // wartość zaznaczonego klucza
        $this->elements = ''; // tablica w postaci 'klucz'=>'wartość' opcji selecta
        $this->separator = ''; // separator pomiędzy elementami w postaci html
        $this->show_ul = false; // czy pokazywać jako ul
        $this->ul_class = ''; // classy dodawanie do ul
        $this->li_class = ''; // classy dodawanie do li
        $this->li_style = ''; // style dodawanie do li
        $this->insert_value = false; // zarządzanie wartością danych poprzez przekazanie typu li
        $this->own_values = ''; // lista własnych wartości
        $this->add_inputs = ''; // lista inputów do wyświetlenia
    }

    /*
     * generujemy i zwracamy input
      */
    function show()
    {
        $id = (!empty($this->field_id)) ? $this->field_id : 'rad';
        $class = ' form_radio ' . $this->field_id . ' ';
        $class .= ($this->required == 1 || $this->required == 'on') ? ' required_field ' : '';
        $class .= $this->add_class;

        $html = ($this->show_ul == true) ? '<ul class="' . $this->ul_class . '">' : '';
        $i = 0;
        foreach ($this->elements AS $key => $val) {
            $i++;
            $ids = $id . '_' . $i;
            $checked = ($this->selected == $key) ? 'checked="checked"' : '';
            if ($i == count($this->elements)) {
                $this->separator = '';
            }
            if ($this->show_ul == true) {
                $html .= '<li class="' . $this->li_class . '" style="' . $this->li_style . '">';
                $this->separator = '</li>';
            }

            if ($this->insert_value == false) {
                $value = $key;
            } else {
                $value = $i;
            }
            $checked = ($this->selected == $value) ? 'checked="checked"' : '';

            $add_input = '';
            if (!empty($this->add_inputs[$i - 1])) {
                $placeholder = ($this->add_inputs[$i - 1] != 1) ? $this->add_inputs[$i - 1] : '';
                foreach ($this->add_inputs_values AS $sk => $sv) {
                    if ($sk == $this->field_id . '_' . $key . '_input') {
                        $sm_value = $sv;
                    }
                }
                $add_input = ' <input type="text" id="' . $ids . '_ainput" name="' . $this->name . '_input" class="check_parent" value="' . $sm_value . '" placeholder="' . $placeholder . '" /> ';
            }
            $html .= '<input type="radio" name="' . $this->name . '" id="' . $ids . '" class="' . $class . '" value="' . $value . '" ' . $checked . ' /> <label for="' . $ids . '">' . $val . '</label>' . $add_input . $this->separator;
        }
        $html .= ($this->show_ul == true) ? '</ul>' : '';
        return $html;
    }
}

/*
 * pole do wysyłania plików
*/

class Input_file extends Form
{
    function __construct()
    {
        /*
          * zmienne używane w polu
         */
        $this->required = ''; // czy wymagane
        $this->add_class = ''; // dodatkowe klasy
        $this->name = ''; // nazwa pola
        $this->field_id = ''; // wartość taga id=""
        $this->scale = ''; // jeżeli wyłaczone wartość '', jeżeli włączone szerokość, wysokość np. '640,480'
        $this->thumbnail = ''; // jeżeli wyłaczone wartość '', jeżeli włączone rozmiar kwadratu np. '100'
        $this->allowed_extension = ''; // dopuszczalne rozszerzenia rozdzielane przecinkami np. 'jpg,pdf,doc'
        $this->max_size = ''; // maksymalny rozmiar pliku
        $this->old_file = ''; // wartość przekazywana poprzedni plik
        $this->is_movie = ''; // czy odtwarzać jako film
        $this->sql_ads = ''; // id pola dla którego jest załącznikiem
    }

    /*
     * generujemy i zwracamy input
      */
    function show()
    {
        $id = (!empty($this->field_id)) ? 'id="' . $this->field_id . '"' : '';
        $class = ' form_input_file ';
        $class .= ($this->required == 1 || $this->required == 'on') ? ' required_field ' : '';
        $class .= $this->add_class;
        $value = (!empty($this->value)) ? 'value="' . stripslashes($this->value) . '"' : '';
        $html = '<input type="file" name="' . $this->name . '" ' . $id . ' class="' . $class . '" />';
        $html .= (!empty($this->allowed_extension)) ? '<input type="hidden" name="allowed_extension_' . $this->field_id . '" value="' . $this->allowed_extension . '" />' : '';
        $html .= (!empty($this->thumbnail)) ? '<input type="hidden" name="thumbnail_' . $this->field_id . '" value="' . $this->thumbnail . '" />' : '';
        $html .= (!empty($this->scale)) ? '<input type="hidden" name="scale_' . $this->field_id . '" value="' . $this->scale . '" />' : '';
        $html .= (!empty($this->is_movie)) ? '<input type="hidden" name="is_movie_' . $this->field_id . '" value="' . $this->is_movie . '" />' : '';
        $html .= (!empty($this->max_size)) ? '<input type="hidden" name="maxsize_' . $this->field_id . '" value="' . $this->max_size . '" />' : '';
        $html .= (!empty($this->old_file)) ? '<input type=\'hidden\' name=\'old_file_' . $this->field_id . '\' value=\'' . $this->old_file . '\' />' : '';
        $html .= (!empty($this->sql_ads)) ? '<input type="hidden" name="ads_' . $this->field_id . '" value="' . $this->sql_ads . '" />' : '';
        return $html;
    }
}

/*
 * pole kalendarza
*/

class Calendar extends Form
{
    function __construct()
    {
        /*
          * dodatkowe pliki css i js używane w polu
         */
        global $site;
        /*$site->js_list = array_merge($site->js_list, array('ui/minified/jquery.ui.datepicker.min','ui/minified/jquery.ui.mouse.min','ui/minified/jquery.ui.position.min','ui/minified/jquery.ui.widget.min','ui/i18n/jquery.ui.datepicker-pl'));*/

        /*
          * zmienne używane w polu
         */
        $this->required = ''; // czy wymagane
        $this->add_class = ''; // dodatkowe klasy
        $this->name = ''; // nazwa pola
        $this->field_id = ''; // wartość taga id=""
        $this->date_format = ''; // format daty
        $this->value = ''; // wartość pola
        $this->min_date = ''; // minimalna wartość wyboru
        $this->max_date = ''; // maksymalna wartość wyboru
        $this->show_button_bar = ''; // pokazuje listwę z przyciskami
        $this->select_month = ''; // pokazuje select dla miesiąca
        $this->select_year = ''; // pokazuje select dla roku
        $this->icon_trigger = ''; // kalendarz startuje po kliknięciu w ikonkę
        $this->readonly = ''; // umożliwienie ręcznego wpisania daty
        $this->placeholder = ''; // pokazuje placeholdera
    }

    /*
     * generujemy i zwracamy input
      */
    function show()
    {
        global $site;
        $this->date_format = (empty($this->date_format)) ? 'dd.mm.yy' : $this->date_format;
        $id = (!empty($this->field_id)) ? 'id="' . $this->field_id . '"' : '';
        $class = ' form_datapicker ';
        $class .= ($this->required == 1 || $this->required == 'on') ? ' required_field ' : '';
        $class .= $this->add_class;
        $value = (!empty($this->value)) ? stripslashes($this->value) : '';
        $min_date = (!empty($this->min_date)) ? 'minDate: \'' . $this->min_date . '\',' : '';
        $max_date = (!empty($this->max_date)) ? 'maxDate: \'' . $this->max_date . '\',' : '';
        $button_bar = ($this->show_button_bar == 1 || $this->show_button_bar == 'on') ? 'showButtonPanel: true,' : '';
        $select_month = ($this->select_month == 1 || $this->select_month == 'on') ? 'changeMonth: true,' : '';
        $select_year = ($this->select_year == 1 || $this->select_year == 'on') ? 'changeYear: true,' : '';
        $icon_trigger = ($this->icon_trigger == 1 || $this->icon_trigger == 'on') ? "showOn: \"both\",\nbuttonImage: \"" . $site->server_config->public_dir->gfx . "form/calendar.gif\"" : "";
        $readonly = ($this->readonly == 1 || $this->readonly == 'on') ? 'readonly="readonly"' : '';
        $placeholder = (!empty($this->placeholder)) ? 'placeholder="' . $this->placeholder . '"' : '';
        $html = '
	 <script type="text/javascript">
	 $(document).ready(function(){
      $("#' . $this->field_id . '").datepicker({
        dateFormat:\'' . $this->date_format . '\',
	     ' . $min_date . '
	     ' . $max_date . '
		  ' . $button_bar . '
		  ' . $select_month . '
		  ' . $select_year . '
		  ' . $icon_trigger . '
      });
	 });
	 </script>
	 <input type="text" name="' . $this->name . '" ' . $id . ' class="' . $class . '" value="' . $value . '" ' . $readonly . ' ' . $placeholder . ' />';
        return $html;
    }
}

/*
 * pole do wstawiania linku
*/

class Insert_link extends Form
{
    function __construct()
    {
        /*
          * zmienne używane w polu
         */
        $this->required = ''; // czy wymagane
        $this->add_class = ''; // dodatkowe klasy
        $this->name = ''; // nazwa pola
        $this->field_id = ''; // wartość taga id=""
        $this->value = ''; // wartość pola
        $this->icon_trigger = ''; // kalendarz startuje po kliknięciu w ikonkę
        $this->readonly = 1; // umożliwienie ręcznego wpisania daty
        $this->allowed_protocols = ''; // umożliwione protokoły oddzielone przecinkami np. 'http://,ftp://'
        $this->width = ''; // szerokość pola
        $this->url_width = 200;
    }

    /*
     * generujemy i zwracamy input
      */
    function show()
    {
        global $site;
        global $translate;
        $id = 'id="' . $this->field_id . '"';
        $class = ' form_url ';
        $class .= ($this->required == 1 || $this->required == 'on') ? ' required_field ' : '';
        $class .= $this->add_class;
        $value = (!empty($this->value)) ? stripslashes($this->value) : '';
        $icon_trigger = ($this->icon_trigger == 1 || $this->icon_trigger == 'on') ? '<img src="' . $site->server_config->public_dir->gfx . 'form/link.png" alt="" class="insert_url linklike" />' : '';
        $readonly = ($this->readonly == 1 || $this->readonly == 'on') ? 'readonly="readonly"' : '';
        $width = ($this->width > 0) ? 'style="width:' . $this->width . 'px"' : '';
        $url_width = ($this->url_width > 0) ? 'style="width:' . $this->url_width . 'px"' : '';

        $allow_protocol = explode(',', $this->allowed_protocols);
        $html = '
    <script type="text/javascript">$(document).ready(function(){insert_link_ready(\'' . $this->field_id . '\');});</script>
	 <input type="text" name="' . $this->name . '" ' . $id . ' class="' . $class . '" value="' . $value . '" ' . $readonly . ' ' . $width . ' />' . $icon_trigger . '
	 <div id="url_box_' . $this->field_id . '" class="url_box" style="display:none">
	   <div class="url_box_body">
		   <span class="close_url_box linklike btn btn-danger"><i class="icon-remove icon-white"></i></span>';
        if (count($allow_protocol) > 1) {
            $html .= '<select class="url_type al_left"><optgroup label="adres statyczny"></optgroup>';
            $html .= (in_array('http', $allow_protocol)) ? '<option value="http://">http://</option>' : '';
            $html .= (in_array('https', $allow_protocol)) ? '<option value="http://">https://</option>' : '';
            $html .= (in_array('email', $allow_protocol)) ? '<option value="mailto:">adres email</option>' : '';
            $html .= (in_array('ftp', $allow_protocol)) ? '<option value="ftp://">adres ftp</option>' : '';
            $html .= '</optgroup>';
            $html .= (array_search('[rel]', $allow_protocol)) ? '<option value="[rel]">adres relatywny</option>' : '';
            $html .= (array_search('[local]', $allow_protocol)) ? '<option value="">adres lokalny</option>' : '';
            $html .= '</select>';
        } else {
            if ($allow_protocol[0] == 'http') {
                $protocol = 'http://';
            } elseif ($allow_protocol[0] == 'email') {
                $protocol = 'mailto:';
            }
            $html .= '<input type="hidden" class="url_type" value="' . $protocol . '">';
        }
        $html .= '<input type="text" class="url_address al_left" ' . $url_width . ' />
			<button class="btn btn-primary add_url al_left">' . $translate->ok . '</button>
			<div class="url_error al_clear"></div>
		</div>
	 </div>';
        return $html;
    }
}

/*
 * pole z ograniczonymi znakami
*/

class Restricted extends Form
{
    function __construct()
    {
        /*
          * zmienne używane w polu
         */
        $this->required = ''; // czy wymagane
        $this->add_class = ''; // dodatkowe klasy
        $this->maxlength = ''; // maksymalna długość
        $this->name = ''; // nazwa pola
        $this->field_id = ''; // wartość taga id=""
        $this->value = ''; // wartość pola
        $this->allow_int = ''; // czy dozwolony są cyfry
        $this->allow_alpha = ''; // czy dozwolone są znaki alfa numeryczne
        $this->allowed_chars = ''; // dozwolone znaki rozdzielone znakiem | np. '&|#|,'
        $this->width = ''; // szerokość pola w px
        $this->style = ''; // styl inputa
        $this->placeholder = '';
    }

    /*
     * generujemy i zwracamy input
      */
    function show()
    {
        $id = 'id="' . $this->field_id . '"';
        $class = ' form_restricted ';
        $class .= ($this->required == 1 || $this->required == 'on') ? ' required_field ' : '';
        $class .= ($this->allow_int == 1 || $this->allow_int == 'on') ? ' allow_int ' : '';
        $class .= ($this->allow_alpha == 1 || $this->allow_alpha == 'on') ? ' allow_alpha ' : '';
        $class .= (!empty($this->allowed_chars)) ? ' allowed_chars ' : '';
        $class .= $this->add_class;
        $width = ($this->width > 0) ? 'width:' . $this->width . 'px' : '';
        $value = (!empty($this->value)) ? stripslashes($this->value) : '';
        $maxlenght = ($this->maxlength > 0) ? 'maxlength="' . $this->maxlength . '"' : '';
        $placeholder = (!empty($this->placeholder)) ? 'placeholder="' . $this->placeholder . '"' : '';
        $html = '
	 <script type="text/javascript">$(document).ready(function(){restricted_ready(\'' . $this->field_id . '\',\'' . $this->allowed_chars . '\');});</script>
	 <input type="text" name="' . $this->name . '" ' . $id . ' class="' . $class . '" value="' . $value . '" ' . $placeholder . ' ' . $maxlenght . ' style="' . $this->style . ';' . $width . '" />';
        return $html;
    }
}

/*
 * pole hidden
*/

class Hidden extends Form
{
    function __construct()
    {
        /*
          * zmienne używane w polu
         */
        $this->name = ''; // nazwa pola
        $this->field_id = ''; // wartość taga id=""
        $this->value = ''; // wartość pola
        $this->text = ''; // treść informacyjna w miejscu inputa
    }

    /*
     * generujemy i zwracamy input
      */
    function show()
    {
        $id = (!empty($this->field_id)) ? 'id="' . $this->field_id . '"' : '';
        $value = (!empty($this->value)) ? stripslashes($this->value) : '';
        $html = $this->text . '<input type="hidden" name="' . $this->name . '" ' . $id . ' value="' . $value . '" />';
        return $html;
    }
}

/*
 * pole loswa wartość
 */

class Random_value extends Form
{
    function __construct()
    {
        /*
          * zmienne używane w polu
         */
        $this->name = ''; // nazwa pola
        $this->field_id = ''; // wartość taga id=""
        $this->size = ''; // długość pola 1 = 13, 2 = 23
        $this->allow_change = ''; // pozwól na zmianę wartości
    }

    /*
     * generujemy i zwracamy input
      */
    function show()
    {
        $id = (!empty($this->field_id)) ? 'id="' . $this->field_id . '"' : '';
        $value = ($this->size == 2) ? str_replace('.', '', uniqid('', true)) : uniqid();
        $type = ($this->allow_change == 1 || $this->allow_change == 'on') ? 'text' : 'hidden';
        $text = ($this->allow_change == 1 || $this->allow_change == 'on') ? '' : stripslashes($value);

        $html = $text . '<input type="' . $type . '" name="' . $this->name . '" ' . $id . ' value="' . stripslashes($value) . '" />';
        return $html;
    }
}

/*
 * pole Tagi
*/

class Insert_tags extends Form
{
    function __construct()
    {
        /*
          * dodatkowe pliki css i js używane w polu
         */
        global $site;
        /*$site->js_list = array_merge($site->js_list, array('ui/minified/jquery.ui.widget.min','ui/minified/jquery.ui.position.min','ui/minified/jquery.ui.autocomplete.min','tags'));*/

        /*
          * zmienne używane w polu
         */
        $this->required = ''; // czy wymagane
        $this->add_class = ''; // dodatkowe klasy
        $this->name = ''; // nazwa pola
        $this->field_id = ''; // wartość taga id=""
        $this->value = ''; // wartość pola
        $this->width = ''; // szerokość pola w px
    }

    /*
     * generujemy i zwracamy input
      */
    function show()
    {
        $id = (!empty($this->field_id)) ? 'id="' . $this->field_id . '"' : '';
        $class = ' form_input tagsInserter';
        $class .= ($this->required == 1 || $this->required == 'on') ? ' required_field ' : '';
        $class .= $this->add_class;
        $maxlenght = ($this->maxlength > 0) ? 'maxlength="' . $this->maxlength . '"' : '';
        $width = ($this->width > 0) ? 'style="width:' . $this->width . 'px"' : '';
        $value = (!empty($this->value)) ? stripslashes($this->value) : '';
        global $tags;
        if (empty($tags)) {
            $tags = new Tags;
        }
        $html = '
	 <script type="text/javascript">
	   if(tags == undefined){
		  var tags = ' . $tags->load_json_tags() . ';
		}
	 </script>
	 <input type="text" name="' . $this->name . '" ' . $id . ' class="' . $class . '" ' . $maxlenght . ' value="' . $value . '" ' . $width . ' />';
        return $html;
    }
}

/*
 * input hasło
*/

class Password extends Form
{
    function __construct()
    {
        /*
          * zmienne używane w polu
         */
        $this->required = ''; // czy wymagane
        $this->add_class = ''; // dodatkowe klasy
        $this->name = ''; // nazwa pola
        $this->field_id = ''; // wartość taga id=""
        $this->value = ''; // wartość value
        $this->width = ''; // szerokość pola w px
    }

    /*
     * generujemy i zwracamy input
      */
    function show()
    {
        $id = (!empty($this->field_id)) ? 'id="' . $this->field_id . '"' : '';
        $class = ' form_input ';
        $class .= ($this->required == 1 || $this->required == 'on') ? ' required_field ' : '';
        $class .= $this->add_class;
        $maxlenght = ($this->maxlength > 0) ? 'maxlength="' . $this->maxlength . '"' : '';
        $value = (!empty($this->value)) ? stripslashes($this->value) : '';
        $width = ($this->width > 0) ? 'style="width:' . $this->width . 'px"' : '';
        $html = '<input type="password" name="' . $this->name . '" ' . $id . ' class="' . $class . '" ' . $maxlenght . ' value="' . $value . '" ' . $width . ' />';
        return $html;
    }
}

/*
 * input szukaj
*/

class Input_search extends Form
{
    function __construct()
    {
        /*
          * zmienne używane w polu
         */
        $this->maxlength = ''; // maksymalna długość
        $this->add_class = ''; // dodatkowe klasy
        $this->name = ''; // nazwa pola
        $this->field_id = ''; // wartość taga id=""
        $this->placeholder = ''; // wartość pola
        $this->width = ''; // szerokość pola w px
        $this->advenced_search = ''; // czy pokazywać zaawsowane szukanie
    }

    /*
     * generujemy i zwracamy input
      */
    function show()
    {
        $id = (!empty($this->field_id)) ? 'id="' . $this->field_id . '"' : '';
        $class = ' form_input serch_input required_field ';
        $class .= $this->add_class;
        $width = ($this->width > 0) ? 'style="width:' . $this->width . 'px"' : '';
        $maxlenght = ($this->maxlength > 0) ? 'maxlength="' . $this->maxlength . '"' : '';
        $placeholder = (!empty($this->placeholder)) ? stripslashes($this->placeholder) : '';
        $html = '<input type="text" name="' . $this->name . '" ' . $id . ' class="' . $class . '" ' . $maxlenght . ' placeholder="' . $placeholder . '" ' . $width . ' />';
        return $html;
    }
}

?>