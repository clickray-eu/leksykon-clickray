<?php
/**
 * @framework 3wymiar.pl 3.0 2002-2013 (c)
 *
 * @author Marcin Cichy <cichy@3wymiar.pl>
 * @version 1.0
 */

class Watchmaker
{
    function __construct()
    {
        global $site;
        $site->js_list[] = 'watchmaker';
    }

    /* sprawdzamy czy przyszłość czy przeszłość */
    function make_time($time)
    {
        global $translate;
        if ($time == 0) {
            return 'nie określono';
        } elseif ($time > time()) {
            return $this->visit_future($time);
        } else {
            return $this->return_past($time);
        }
    }

    /* lecimy w przyszłość */
    function visit_future($time)
    {
        global $translate;
        $dif = $time - time();
        if ($time == 0) {
            return '--';
        } elseif ($dif == 0) {
            return '<span class="watchmaker_future" id="' . $time . '">teraz</span>';
        } elseif ($dif < 60) {
            return '<span class="watchmaker_future" id="' . $time . '">mniej niż minutę</span>';
        } elseif ($dif < (60 * 60)) {
            return '<span class="watchmaker_future" id="' . $time . '">za ' . $this->find_variant_minutes(ceil($dif / 60), $time) . '</span>';
        } elseif (date('dmy') == date('dmy', $time)) {
            return 'dziś o ' . date('H:i', $time);
        } elseif (date('dmy', strtotime('tomorrow')) == date('dmy', $time)) {
            return 'jutro o ' . date('H:i', $time);
        } /*elseif($dif < (60*60*24*7)){
	   return $this->find_variant_days(ceil($dif/(60*60*24)), $time, $translate->za, '', false);
	 }
	 elseif($time < strtotime('+1 year')){
	   return $this->find_variant_months($time, $translate->za, '');
	 }
	 elseif($time < strtotime('+10 years')){
	   return $this->find_variant_years($time, $translate->za, '');
	 }*/
        else {
            return date('d.m.Y H:i', $time);
        }
    }

    /* wracamy do przyszłości */
    function return_past($time)
    {
        global $translate;
        $dif = time() - $time;
        if ($dif == 0) {
            return '<span class="watchmaker" id="' . $time . '">teraz</span>';
        } elseif ($dif < 60) {
            return '<span class="watchmaker" id="' . $time . '">więcej niż minutę temu</span>';
        } elseif ($dif < (60 * 60)) {
            return '<span class="watchmaker" id="' . $time . '">' . $this->find_variant_minutes(ceil($dif / 60), $time) . ' temu</span>';
        } elseif (date('dmy') == date('dmy', $time)) {
            return 'dziś o ' . date('H:i', $time);
        } elseif (date('dmy', strtotime('yesterday')) == date('dmy', $time)) {
            return 'wczoraj o ' . date('H:i', $time);
        } /*
	 elseif($dif < (60*60*24*7)){
	   return $this->find_variant_days(ceil($dif/(60*60*24)), $time, '', $translate->temu, true);
	 }
	 elseif($time > strtotime('-1 year')){
	   return $this->find_variant_months($time, '', $translate->temu);
	 }
	 elseif($time > strtotime('-10 years')){
	   return $this->find_variant_years($time, '', $translate->temu);
	 }
	 */
        else {
            return date('d.m.Y H:i', $time);
        }
    }

    /* odmiana końcówki dla minut */
    function find_variant_minutes($nr, $time)
    {
        global $translate;
        if ($nr == 1) {
            return 'minute';
        } elseif ($nr < 5) {
            return $nr . ' minut';
        } elseif ($nr < 22) {
            return $nr . ' minut';
        } elseif (substr($nr, -1) > 1 && substr($nr, -1) < 5) {
            return $nr . ' minuty';
        } else {
            return $nr . ' minut';
        }
    }

    /* odmiana końcówki dla dni */
    /*function find_variant_days($nr, $time, $przed, $konc, $przed_dzien){
      $day_names = 	array('poniedziałek','wtorek','środa','czwartek','piątek','sobota','niedziela');
       $day_prev = array('w zeszły','w zeszły','zeszła','w zeszły','w zeszły','zeszła','zeszła');
       if($nr < 8){
         $przed_dzien = ($przed_dzien == true)?$day_prev[date('N',$time)-1]:'';
         return $przed_dzien.' '.$day_names[date('N',$time)-1].' o '.date('H:i', $time);
       }
       elseif($nr > 7 && $nr < 15){
         return $przed.' ponad tydzień '.$konc;
       }
       elseif($nr > 14 && $nr < 22){
         return $przed.' ponad dwa tygodnie '.$konc;
       }
       elseif($nr > 21 && $nr < 29){
         return $przed.' ponad trzy tygodnie '.$konc;
       }
       else{
         return $przed.' ok. miesiąc '.$konc;
       }
    }*/

    /* odmiana końcówki dla miesięcy */
    /*function find_variant_months($time, $przed, $konc){
       if($time > strtotime('-2 month')){
         return $przed.' ponad miesiąc '.$konc;
       }
       elseif($time > strtotime('-3 month')){
         return $przed.' ponad 2 miesiące '.$konc;
       }
       elseif($time > strtotime('-4 month')){
         return $przed.' ponad 3 miesięce '.$konc;
       }
       elseif($time > strtotime('-5 month')){
         return $przed.' ponad 4 miesięce '.$konc;
       }
       elseif($time > strtotime('-6 month')){
         return $przed.' ponad 5 miesięcy '.$konc;
       }
       elseif($time > strtotime('-7 month')){
         return $przed.' ponad pół roku '.$konc;
       }
       elseif($time < strtotime('-1 year')){
         return $przed.' prawie rok '.$konc;
       }
       else{
         return $przed.' ok. rok '.$konc;
       }
    }*/

    /* odmiana końcówki dla lat */
    /*function find_variant_years($time, $przed, $konc){
       if($time > strtotime('-2 years')){
         return $przed.' ponad rok '.$konc;
       }
       elseif($time > strtotime('-3 month')){
         return $przed.' ponad 2 lata '.$konc;
       }
       elseif($time > strtotime('-4 month')){
         return $przed.' ponad 3 lata '.$konc;
       }
       elseif($time > strtotime('-5 month')){
         return $przed.' ponad 4 lata '.$konc;
       }
       elseif($time > strtotime('-6 month')){
         return $przed.' ponad 5 lat '.$konc;
       }
       elseif($time > strtotime('-7 month')){
         return $przed.' ponad połowa dekady '.$konc;
       }
       elseif($time < strtotime('-1 year')){
         return $przed.' prawie dekada '.$konc;
       }
       else{
         return $przed.' ok. dekada '.$konc;
       }
    }*/
}