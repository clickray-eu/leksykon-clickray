<?php
/**
 * @framework 3wymiar.pl 3.0 2002-2013 (c)
 *
 * @author Marcin Cichy <cichy@3wymiar.pl>
 * @version 1.0
 */

class Alerts
{
    function __construct()
    {
        /*
          * zmienne używane w klasie
         */
        $this->text = ''; // treść komunikatu
        $this->type = ''; // typ komunikatu
    }
}