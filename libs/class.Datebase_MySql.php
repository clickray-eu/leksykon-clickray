<?php
/**
 * @framework 3wymiar.pl 3.0 2002-2013 (c)
 *
 * @author Marcin Cichy <cichy@3wymiar.pl>
 * @version 1.0
 */

class Datebase_MySql
{
    function __construct($sql_config)
    {
        $this->connect($sql_config);
    }

    /*
     * łączymy się z bazą
      * w przypadku braku połączenia zwracamy error
      */

    private function connect($sql_config)
    {
        global $dbh;
        try {
            /*
              * połaczenie z bazą oraz ustawienie kodowania znaków
             */
            $charset = strtolower(str_replace('-', '', $sql_config->charset));
            $dbh = new PDO('mysql:host=' . $sql_config->host . ';dbname=' . $sql_config->database . ';charset=' . $charset, $sql_config->user, $sql_config->password);
            $dbh->query('SET character_set_connection=' . $charset);
            $dbh->query('SET character_set_client=' . $charset);
            $dbh->query('SET character_set_results=' . $charset);
        } catch (PDOException $e) {
            $this->error_handling($e->getMessage());
        }
    }

    /*
      * zwracamy jedną wartość z bazy w postaci zmiennej
      */
    public function get_var($sql_query)
    {
        global $dbh;
        $prepare = $dbh->query($sql_query);
        /*
         * sprawdzamy czy zapytanie jest poprawne, jeżeli nie zwracamy error
        */
        if (!$prepare) {
            $error = $dbh->errorInfo();
            $this->error_handling($error[2]);
        } else {
            /*
              * zwracamy pierwszy wynik w postaci zmiennej
             */
            $row = $prepare->fetch();
            return $row[0];
        }
    }

    /*
      * zwracamy jeden wiersz z bazy w postaci obiektów
      */

    public function get_row($sql_query)
    {
        global $dbh;
        $row = '';
        $prepare = $dbh->query($sql_query);
        /*
         * sprawdzamy czy zapytanie jest poprawne, jeżeli nie zwracamy error
        */
        if (!$prepare) {
            $error = $dbh->errorInfo();
            $this->error_handling($error[2]);
        } else {
            /*
              * tworzymy obiekty z nazwami kolumny i zwracamy
             */
            foreach ($prepare->fetch() AS $key => $val) {
                if (!is_int($key)) {
                    $row->$key = $val;
                }
            }
            return $row;
        }
    }

    /*
      * zwracamy tablicę z wynikami w postaci obiektów
      */

    public function get_results($sql_query)
    {
        global $dbh;
        $prepare = $dbh->query($sql_query);
        /*
         * sprawdzamy czy zapytanie jest poprawne, jeżeli nie zwracamy error
        */
        if (!$prepare) {
            $error = $dbh->errorInfo();
            $this->error_handling($error[2]);
        } else {
            /*
              * tworzymy tablicę zawierającą obiekty z nazwami kolumn
             */
            $rows = array();
            foreach ($prepare->fetchAll() AS $q) {
                $row = '';
                foreach ($q AS $key => $val) {
                    if (!is_int($key)) {
                        $row->$key = $val;
                    }
                }
                if (!empty($row->id)) {
                    $rows[$row->id] = $row;
                } else {
                    $rows[] = $row;
                }
            }
            return $rows;
        }
    }

    /*
      * testujemy czy select jest poprawny
      */

    public function test_results($sql_query)
    {
        global $dbh;
        $prepare = $dbh->query($sql_query);
        /*
         * sprawdzamy czy zapytanie jest poprawne, jeżeli nie zwracamy error
        */
        if (!$prepare) {
            return false;
        } else {
            return true;
        }
    }

    /*
      * zapisujemy lub nadpisujemy rekord
      */

    public function query($sql_query)
    {
        global $dbh;
        /*
         * sprawdzamy czy jest to INSERT czy UPDATE
         * innych komend nie dopuszczamy
        */
        $check = explode(' ', $sql_query);
        switch (strtoupper($check[0])) {
            case 'UPDATE':
                $method = 'update';
                break;
            case 'INSERT':
                $method = 'insert';
                break;
            default:
                $this->error_handling('nieprawidłowa komenda ' . $check[0]);
                break;
        }
        $prepare = $dbh->query($sql_query);
        /*
         * sprawdzamy czy zapytanie jest poprawne, jeżeli nie zwracamy error
        */
        if (!$prepare) {
            $error = $dbh->errorInfo();
            $this->error_handling($error[2]);
        } else {
            /*
             * jeżeli jest to insert zwracamy id wstawionego rekordu
            */
            if ($method == 'insert') {
                return $dbh->lastInsertId();
            }
        }
    }

    /*
     * obsługa errorów
      */

    private function error_handling($error)
    {
        print "DB Error!: " . $error . "<br/>";
        die();
    }
}

?>