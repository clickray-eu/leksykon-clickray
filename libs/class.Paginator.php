<?php
/**
 * @framework 3wymiar.pl 3.0 2002-2013 (c)
 *
 * @author Marcin Cichy <cichy@3wymiar.pl>
 * @version 1.0
 */

class Paginator
{
    function __construct()
    {
        /*
          * zmienne używane przy stronicowaniu
         */
        $this->start = ''; // aktualna strona na której jesteśmy
        $this->elements_on_page = ''; // ile elemenetów na stronie
        $this->radius = ''; // promień ile pokazywać przycisków
        $this->count_rows = ''; // suma elementów w całym zapytaniu (bez LIMIT)
        $this->link_class = ''; // klasa dla linków
        $this->active_class = ''; // klasa dla aktywnego elementu
    }

    function content_html()
    {
        return '<div><ul class="pagination"><li>' . $this->paginator_elements() . '</li></ul></div>';
    }

    function paginator_elements()
    {
        global $site;

        $this->count_pages = ceil($this->count_rows / $this->elements_on_page);
        $this->active_class = "active lred";
        if ($_SERVER['REQUEST_URI'][strlen($_SERVER['REQUEST_URI']) - 1] == '/') {
            $_SERVER['REQUEST_URI'] = substr($_SERVER['REQUEST_URI'], 0, -1);
        }
        $uri_array = explode('/', $_SERVER['REQUEST_URI']);
        if (is_numeric($uri_array[count($uri_array) - 1])) {
            unset($uri_array[count($uri_array) - 1]);
        }
        $uri = implode('/', $uri_array) . '/';
        $html = ($this->start > 0) ? '<li><a href="' . $uri . ($this->start - 1) . '" class="' . $this->link_class . '">&#171;</a></li>' : '';
        $html .= ($this->start - $this->radius > 0) ? '<li><a href="' . $uri . (0) . '" class="' . $this->link_class . '">1</a></li>' : '';
        $html .= ($this->start - $this->radius > 1) ? '<li class="disabled"><a herf=""> ... </a></li>' : '';

        for ($i = ($this->start - $this->radius); $i <= $this->start + $this->radius; $i++) {
            if ($i == $this->start) {
                $html .= '<li class="' . $this->active_class . '"><a href="#" class="' . $this->link_class . '">' . ($i + 1) . '</a></li>';
            } elseif ($i > -1 && $i < $this->count_pages) {
                $html .= '<li><a href="' . $uri . ($i) . '" class="' . $this->link_class . '">' . ($i + 1) . '</a></li>';
            }
        }

        $html .= ($this->start + $this->radius < ($this->count_pages - 2)) ? '<li class="disabled"><a herf=""> ... </a></li>' : '';
        $html .= ($this->start + $this->radius < $this->count_pages - 1) ? '<li><a href="' . $uri . ($this->count_pages - 1) . '" class="' . $this->link_class . '">' . ($this->count_pages) . '</a></li>' : '';
        $html .= ($this->start < ($this->count_pages - 1)) ? '<li><a href="' . $uri . ($this->start + 1) . '" class="' . $this->link_class . '">&#187;</a></li>' : '';
        return $html;
    }
}