<?php
/**
 * @framework 3wymiar.pl 3.0 2002-2013 (c)
 *
 * @author Marcin Cichy <cichy@3wymiar.pl>
 * @version 1.0
 */

class Config
{
    function __construct()
    {
        $this->charset = 'UTF-8'; // kodowanie znaków w serwisie i bazie danych
        define('O3w_VER', '1.0.0');
    }

    /*
     * ustawienia mysql
      */
    function get_sql_config()
    {
        $this->sql->host = 'localhost'; //adres hosta mysql
        $this->sql->database = 'clickray_leksykon'; // nazwa bazy mysql
        $this->sql->user = 'clickray'; // nazwa użytkownika mysql
        $this->sql->password = '5PIQ0Mx3qzV8H9Bj'; // hasło użytkownika mysql
        $this->sql->charset = $this->charset;
        return $this->sql;
    }

    /*
      * ustawienia serwera
      */
    function get_server_config()
    {
        $this->server->public_dir->css = 'public/css/'; // adres plików css
        $this->server->public_dir->js = 'public/js/'; // adres plików javascript
        $this->server->public_dir->gfx = 'public/gfx/'; // adres grafik
        $this->server->public_dir->host = '10.0.0.92/leksykon-clickray/';
        return $this->server;
    }

    /*
     * ustawienia serwisu
      */
    function get_service_config()
    {
//        $this->service = new \stdClass();
        $this->service->load_options_from_sql = true;
        $this->service->global_title = 'clickray.pl'; // globalna nazwa serwisu pojawiajająca się w <title>
        $this->service->global_separator = ' %7C '; // separator pomiędzy składowymi w  &#187;<title>
        $this->service->charset = $this->charset;
        $this->service->lang = 'pl'; // domyślny język aplikacji
        $this->service->main_page = 'home'; // strona główna aplikacji
        $this->service->template_file = 'full_page'; // nazwa wgrywanego widoku serwisu
        $this->service->currency = 'PLN'; // waluta serwisu
        $this->service->dotpay_id = ''; // id płatności d
        //$this->service->admin_login = 'clickrayadmin'; // login do panelu admina
        //$this->service->admin_pass = 'k42j3kljf0943'; // hasło do panelu admina

        return $this->service;
    }
}

?>