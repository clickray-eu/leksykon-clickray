<?php
/**
 * @framework 3wymiar.pl 3.0 2002-2013 (c)
 *
 * @author Marcin Cichy <cichy@3wymiar.pl>
 * @version 1.0
 */

class Bootstrap
{
    function __construct()
    {
    }
}

class Accordion extends Bootstrap
{
    function __construct()
    {
        $this->id; //accordion id
        $this->elements;
        /*
         * array treści w postaci:
         * var->header (nagłówek)
         * var->id (id elementu)
         * var->body (treść elementu)
         * var->class (nazwy klas)
        */
    }

    function show()
    {
        $this->id = (!empty($this->id)) ? $this->id : uniqid();
        $html = '<div class="accordion" id="' . $this->id . '">';
        foreach ($this->elements AS $key => $var) {
            $html .= '<div class="accordion-group">
		    <div class="accordion-heading"><a class="accordion-toggle" data-toggle="collapse" data-parent="#' . $this->id . '" href="#' . $var->id . '">' . $var->header . '</a></div>
          <div id="' . $var->id . '" class="accordion-body ' . $this->class . '"><div class="accordion-inner">' . $var->body . '</div></div>
		  </div>';
        }
        $html .= '</div>';
        return $html;
    }
}

?>