<?php
/**
 * @framework 3wymiar.pl 3.0 2002-2012 (c)
 *
 * @author Marcin Cichy <cichy@3wymiar.pl>
 * @version 1.0
 */


class superadmin
{
    function __construct()
    {
        /* zmienne definiowane w pliku inicjacyjnym */
        $this->table_name = '';
        $this->table_sort = "id";
        $this->use_name = '';
        $this->table_headers = '';
        $this->insert = '';
        $this->site_url = '';
        $this->add_to_form = '';
        $this->add_to_options = '';
        /* upload obrazków */
        $this->form_enctype = '';
        $this->upload_id = '';
        $this->upload_method = '';
        $this->upload_dir = '';
        $this->upload_extension = array(); // podajemy wartość numeryczną pliku
        $this->upload_size = array(); // [0] => szerokość, [1] => wysokość
        /* data */
        $this->make_unix = false;
        /* galeria */
        $this->make_gallery = false;
        $this->gallery_dir = '';
        $this->make_upload = false;
        /* pozycja */
        $this->change_position = false;
        $this->position_value = ''; // value (wyświetlana nazwa)
        /* wyszukiwanie */
        $this->show_search = false;
        $this->search_field = '';
    }

    /* dodaj nowy rekord */
    function add_new()
    {
        global $sql;

        $config = new Config();
        $insert_data = "id";
        $insert_vals = "'0'";
        foreach ($this->insert AS $key => $v) {
            if (!empty($_POST[$key])) {
                if ($v[0] == 'calendar' && $this->make_unix == true) {
                    $d = explode('.', $_POST[$key]);
                    $time = mktime(0, 1, 1, $d[1], $d[0], $d[2]);
                    $_POST[$key] = $time;
                }

                if ($v[0] != 'file') {
                    $insert_data .= ",`" . $key . "`";
                    $insert_vals .= ",'" . mysqli_escape_string(mysqli_connect($config->get_sql_config()->host, $config->get_sql_config()->user, $config->get_sql_config()->password, $config->get_sql_config()->database), $_POST[$key]) . "'";
                }
            }
        }
        $this->upload_id = $sql->query("INSERT INTO " . $this->table_name . " (" . $insert_data . ") VALUES (" . $insert_vals . ")");

        if ($this->make_gallery == true) {
            if (is_array($this->gallery_dir)) {
                foreach ($this->gallery_dir AS $dir) {
                    mkdir(dirname(__FILE__) . '/../' . $dir . $this->upload_id);
                    chmod(dirname(__FILE__) . '/../' . $dir . $this->upload_id, 0777);
                }
            } else {
                mkdir(dirname(__FILE__) . '/../' . $this->gallery_dir . $this->upload_id);
                chmod(dirname(__FILE__) . '/../' . $this->gallery_dir . $this->upload_id, 0777);
            }
        }

        if ($this->upload_method == 'copy' && !empty($_FILES)) {
            $this->upload_copy();
        }

        $_SESSION['alert']->type = 'success';
        $_SESSION['alert']->text = 'poprawnie dodano ' . $this->use_name;
        header('location: ' . $this->site_url);
        exit();
    }

    /* nadpisz rekord */
    function save_form()
    {
        global $sql;

        $config = new Config();
        $insert_data = "";
        foreach ($this->insert AS $key => $v) {
            //if(!empty($_POST[$key])){
            if ($v[0] == 'calendar' && $this->make_unix == true) {
                $d = explode('.', $_POST[$key]);
                $time = mktime(0, 1, 1, $d[1], $d[0], $d[2]);
                $_POST[$key] = $time;
            }

            if ($v[0] != 'file') {
                $insert_data .= "`" . $key . "` = '" . mysqli_escape_string(mysqli_connect($config->get_sql_config()->host, $config->get_sql_config()->user, $config->get_sql_config()->password, $config->get_sql_config()->database), $_POST[$key]) . "', ";
            }
            //}
        }
        $insert_data = substr($insert_data, 0, -2);

        $this->upload_id = intval($_POST['id']);
        if ($this->upload_method == 'copy' && !empty($_FILES)) {
            $this->upload_copy();
        }

        $sql->query("UPDATE " . $this->table_name . " SET " . $insert_data . " WHERE id = '" . intval($_POST['id']) . "'");
        $_SESSION['alert']->type = 'success';
        $_SESSION['alert']->text = 'poprawnie zmieniono ' . $this->use_name;
        header('location: ' . $this->site_url);
        exit();
    }

    /* skasuj rekord */
    function delete_row()
    {
        global $sql;
        if (intval($_GET['id']) > 0) {
            $sql->query("UPDATE " . $this->table_name . " SET deleted_at = UNIX_TIMESTAMP() WHERE id = '" . intval($_GET['id']) . "'");
            $_SESSION['alert']->type = 'alert';
            $_SESSION['alert']->text = 'poprawnie skasowano ' . $this->use_name;
            header('location: ' . $this->site_url);
            exit();
        } else {
            $_SESSION['alert']->type = 'error';
            $_SESSION['alert']->text = 'nieprawidłowe id rekordu';
            header('location: ' . $this->site_url);
            exit();
        }
    }

    /* formularz dodawania / zmieniania rekordu */
    function construct_form()
    {
        global $form;

        if ($_GET['action'] == 'edit') {
            global $sql;
            $old = $sql->get_row("SELECT * FROM " . $this->table_name . " WHERE id = '" . intval($_GET['id']) . "'");
        }

        $html = '';
        foreach ($this->insert AS $key => $ins) {
            if ($ins[0] == 'html') {
                $this->input[$key] = new Textarea;
                $this->input[$key]->width = ($ins[5] > 0) ? $ins[5] : 740;
                $this->input[$key]->height = ($ins[6] > 0) ? $ins[6] : 500;
                $this->input[$key]->name = $key;
                $this->input[$key]->field_id = $key;
                $this->input[$key]->show_wysiwyg = 1;
                $this->input[$key]->value = $old->$key;
            } elseif ($ins[0] == 'file') {
                $this->input[$key] = new Input_file;
                $this->input[$key]->field_id = $key;
                $this->input[$key]->name = $key;
            } else {
                $this->input[$key] = new $ins[0];
                $this->input[$key]->name = $key;
                $this->input[$key]->field_id = $key;
                $this->input[$key]->autocomplete = 0;
                if ($ins[2] == 1) {
                    $this->input[$key]->required = 1;
                }

                if ($ins[0] == 'select') {
                    if (empty($ins[3][0])) {
                        $this->input[$key]->first_value = array('wybierz');
                    }
                    $this->input[$key]->elements = $ins[3];
                    $this->input[$key]->selected = $old->$key;
                } else {
                    if ($ins[0] == 'calendar' && $this->make_unix == true) {
                        $this->input[$key]->date_format = 'dd.mm.yy';
                    } elseif ($ins[0] == 'restricted') {
                        $this->input[$key]->allow_int = 1;
                    }
                    $this->input[$key]->width = ($ins[5] > 0) ? $ins[5] : 500;
                    $this->input[$key]->value = $old->$key;
                }
                //$this->input[$key]->height = 100;
            }
            $html .= '
		<div class="row1"><h3>' . $ins[1] . '</h3></div>
		<div class="row2">' . $ins[4];
            $html .= (!empty($ins[4])) ? '<br />' : '';
            $html .= $this->input[$key]->show();
            $html .= (!empty($ins[4])) ? '</div>' : '';
            $html .= '</div>';
        }

        $html .= $this->add_to_form;

        $text = ($_GET['action'] == 'add') ? 'dodaj' : 'zmień';
        $text .= ' ' . $this->use_name;
        if ($_GET['action'] == 'edit') {
            $html .= '<input type="hidden" name="id" value="' . $_GET['id'] . '" />';
        }
        $html .= '<input type="hidden" name="action" value="' . $_GET['action'] . '" />
	   <p class="al_clear">&nbsp;</p>
	   <hr />
		<div class="row1"></div>
		<div class="row2"><button class="btn btn-success"><i class="icon-white icon-share-alt"></i> ' . $text . '</button></div>
		<p class="al_clear">&nbsp;</p>
	 ';
        return '<form action="' . $this->site_url . '" method="POST" ' . $this->form_enctype . '>' . $html . '</form>';
    }

    /* listowanie rekordów */
    function show_table()
    {
        global $sql;
        global $site;
        $watchmaker = new Watchmaker;
        $config = new Config();
        $html = '';

        if (!empty($this->insert)) {
            $html .= '<a href="' . $this->site_url . '&action=add" class="btn btn-success"><i class="icon-white icon-plus"></i> dodaj ' . $this->use_name . '</a>';
            if ($this->change_position == true) {
                $html .= ' <a href="' . $this->site_url . '&action=position" class="btn btn-info"><i class="icon-white icon-resize-vertical"></i> zmień kolejność wyświetlania</a>';
            }

            if ($this->show_search == true) {
                $html .= '
                  <div class="al_right">
                    <form action="' . $this->site_url . '" method="GET">
                       <input type="hidden" name="go" value="' . $_GET['go'] . '">
                      <div class="row2"><input type="text" name="search" value="" placeholder="szukaj"></div>
                       <div class="row2"><button class="btn"><i class="icon-search"></i></button></div>
                     </form>
                  </div>
                  <div class="al_clear"></div>
		  ';
            }

            $html .= '<hr />';
        }

        $html .= '
             <table class="table table-hover table-striped">
               <thead>
                  <tr>';
        $query = 'id';
        foreach ($this->table_headers AS $name => $table_field) {
            $add = ($name == 'mail') ? 'style="width:800px"' : '';
            $html .= '<th ' . $add . '>' . $name . '</th>';
            if (is_array($table_field) && !is_array($table_field[0]) && is_array($table_field[1])) {
                $query .= ", " . $table_field[0];
            } elseif (is_array($table_field)) {
                foreach ($table_field AS $f) {
                    $query .= ", " . $f;
                }
            } elseif ($table_field != 'options') {
                $query .= ", " . $table_field;
            }
        }
        $html .= '</tr>
            </thead>
            <tbody>';

        $where = '';
        if (!empty($_GET['search'])) {
            $where = " " . $this->search_field . " LIKE '%" . mysqli_escape_string(mysqli_connect($config->get_sql_config()->host, $config->get_sql_config()->user, $config->get_sql_config()->password, $config->get_sql_config()->database), $_GET['search']) . "%' AND ";
        }

        $paginator = new Paginator;
        $paginator->start = intval($_GET['start']);
        $paginator->elements_on_page = 99999;
        $paginator->radius = 1;
        $paginator->count_rows = $sql->get_var("SELECT COUNT(id) FROM " . $this->table_name . " WHERE " . $where . " deleted_at = '0'");
        $paginator->link_class; // klasa dla linków
        $paginator->active_class = 'active';

        $row = $sql->get_results("SELECT " . $query . " FROM " . $this->table_name . " WHERE " . $where . " deleted_at = '0' ORDER BY " . $this->table_sort . " LIMIT " . ($paginator->start * $paginator->elements_on_page) . ", " . $paginator->elements_on_page);
        if (!empty($row)) {
            foreach ($row AS $var) {
                $html .= '<tr>';
                foreach ($this->table_headers AS $name => $table_field) {
                    if ($table_field == 'options') {
                        $width = 200;
                        if (!empty($this->add_to_options)) {
                            for ($i = 0; $i < substr_count($this->add_to_options, '<a href'); $i++) {
                                $width += 100;
                            }
                        }

                        $upload_button = '';
                        if ($this->make_upload == true) {
                            $width += 100;
                            if (file_exists(dirname(__FILE__) . '/../upload/' . $var->id)) {
                                $upload_button = '<a href="' . $this->site_url . '&action=upload&id=' . $var->id . '" class="btn btn-warning"><i class="icon-upload icon-white"></i> upload z FTP</a>';
                            } else {
                                $upload_button = '<a href="#" class="btn btn-warning" disabled="disabled"><i class="icon-upload icon-white"></i> upload z FTP</a>';
                            }
                        }
                        $html .= '<td style="width:' . $width . 'px">' . $upload_button . ' ' . str_replace('[id]', $var->id, $this->add_to_options) . '<a href="' . $this->site_url . '&action=edit&id=' . $var->id . '" class="btn btn-info"><i class="icon-white icon-edit"></i> zmień</a> <a href="' . $this->site_url . '&action=delete&id=' . $var->id . '" class="btn btn-danger deleteThis" title="skasuj ' . $this->use_name . '"><i class="icon-white icon-trash"></i> skasuj</a></td>';
                    } elseif (is_array($table_field) && !is_array($table_field[0]) && is_array($table_field[1])) {
                        if (is_array(json_decode($var->$table_field[0]))) {
                            $html .= '<td>';
                            foreach (json_decode($var->$table_field[0]) AS $nid) {
                                $html .= ' ' . $table_field[1][$nid]->name . ', ';
                            }
                            $html = substr($html, 0, -2);
                            $html .= '</td>';
                        } else {
                            $html .= '<td>' . $table_field[1][$var->$table_field[0]]->name . '</td>';
                        }
                    } elseif ($table_field == 'parent_id') {
                        $html .= '<td>' . $this->parent_list[$var->parent_id]->value . '</td>';
                    } elseif ($table_field == 'show_to') {
                        $html .= '<td>' . $watchmaker->make_time($var->$table_field) . '</td>';
                    } elseif ($table_field == 'send_date') {
                        $html .= '<td>' . $watchmaker->make_time($var->$table_field) . '</td>';
                    } elseif ($table_field == 'url') {
                        $html .= '<td><a href="http://' . $site->server_config->public_dir->host . '/' . $var->$table_field . '.html">http://' . $site->server_config->public_dir->host . '/' . $var->$table_field . '.html</a></td>';
                    } elseif (is_array($table_field)) {
                        $html .= '<td>';
                        $i = 0;
                        foreach ($table_field AS $f) {
                            $i++;
                            if ($f == 'url') {
                                $var->$f = '<a href="http://' . $site->server_config->public_dir->host . '/' . $var->$f . '.html">http://' . $site->server_config->public_dir->host . '/' . $var->$f . '.html</a>';
                            }
                            $html .= ($i == 1) ? '<h3>' . $var->$f . '</h3>' : '<p><i>' . $var->$f . '</i></p>';
                        }
                        $html .= '</td>';
                    } else {
                        $html .= '<td>' . $var->$table_field . '</td>';
                    }
                }
                $html .= '</tr>';
            }
        } else {
            if (!empty($_GET['search'])) {
                $html .= '<tr><td colspan="' . count($this->table_headers) . '"><div class="alert alert-warning">brak znalezionych elementów</div></td></tr>';
            } else {
                $html .= '<tr><td colspan="' . count($this->table_headers) . '"><div class="alert alert-warning">brak dodanych elementów</div></td></tr>';
            }
        }
        $html .= '</tbody>
		<tfoot>';
        if ($paginator->count_rows > $paginator->elements_on_page) {
            $html .= '<tr><td colspan="' . count($this->table_headers) . '">' . $paginator->content_html() . '</td></tr>';
        }
        $html .= '</tfoot>
	 </table>';
        return $html;
    }

    /* uploadowanie pliku z formularza */
    function upload_copy()
    {
        foreach ($this->insert AS $key => $val) {
            if ($val[0] == 'file' && !empty($_FILES[$key]['tmp_name'])) {
                $info = getimagesize($_FILES[$key]['tmp_name']);
                if ($info[2] == 1 && (in_array(1, $this->upload_extension) || empty($this->upload_extension))) {
                    $ext = 'gif';
                } elseif ($info[2] == 2 && (in_array(2, $this->upload_extension) || empty($this->upload_extension))) {
                    $ext = 'jpg';
                } elseif ($info[2] == 3 && (in_array(3, $this->upload_extension) || empty($this->upload_extension))) {
                    $ext = 'png';
                } elseif ($info[2] == 4 && (in_array(4, $this->upload_extension) || empty($this->upload_extension))) {
                    $ext = 'swf';
                } else {
                    $_SESSION['alert']->type = 'warning';
                    $_SESSION['alert']->text = 'zapisano ' . $this->use_name . ' ale wystąpił problem z wgrywanym plikiem.<p>Błędny format pliku.</p>';
                    header('location: ' . $this->site_url . '&action=edit&id=' . $this->upload_id);
                    exit();
                }

                if (!empty($this->upload_size) && ($info[0] != $this->upload_size[0] || $info[1] != $this->upload_size[1])) {
                    $_SESSION['alert']->type = 'warning';
                    $_SESSION['alert']->text = 'zapisano ' . $this->use_name . ' ale wystąpił problem z wgrywanym plikiem.<p>Błędne wymiary pliku.</p>';
                    header('location: ' . $this->site_url . '&action=edit&id=' . $this->upload_id);
                    exit();
                }

                move_uploaded_file($_FILES[$key]['tmp_name'], dirname(__FILE__) . '/../' . $this->upload_dir . $this->upload_id . '.' . $ext);
            }
        }
    }

    /* zmiana pozycji elementu */
    function change_elements_position()
    {
        global $site;
        global $sql;
        $site->js_list[] = 'ui/minified/jquery.ui.widget.min';
        $site->js_list[] = 'ui/minified/jquery.ui.mouse.min';
        $site->js_list[] = 'ui/minified/jquery.ui.sortable.min';
        $site->js_list[] = 'sort_position';

        $row = $sql->get_results("SELECT id, " . $this->position_value . ", position FROM " . $this->table_name . " WHERE deleted_at = '0' ORDER BY position ASC");
        $html = '<div class="alert alert-info">Złap i przeciągnij element na dowolne miejsce.<p>Po zmianie pozycji elementów należy ją zapisać</p></div>
	 <form action="' . $this->site_url . '" method="POST">
	   <ul id="sort_records" class="nav nav-tabs nav-stacked">';
        foreach ($row AS $var) {
            $html .= '<li class="active"><a href="#">' . stripslashes($var->value) . '</a><input type="hidden" name="pos_' . $var->id . '" value="' . $var->position . '"></li>';
        }
        $html .= '</ul>
	   <hr />
		<button class="btn btn-danger" id="save_positions" style="display:none"><i class="icon-white icon-warning-sign"></i> zapisz nowe pozycje</button>
		<input type="hidden" name="action" value="save_positions" />
	 </form>
	 ';
        return $html;
    }

    /* zapis zmiany pozycji */
    function save_elements_position()
    {
        global $sql;
        foreach ($_POST AS $key => $val) {
            $exp = explode('_', $key);
            if ($exp[0] == 'pos') {
                $sql->query("UPDATE " . $this->table_name . " SET position = '" . intval($val) . "' WHERE id = '" . intval($exp[1]) . "'");
            }
        }
        $_SESSION['alert']->type = 'success';
        $_SESSION['alert']->text = 'zapisano nową kolejność wyświetlania';
        header('location: ' . $this->site_url);
        exit();
    }

    function rand_string($length)
    {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        return substr(str_shuffle($chars), 0, $length);
    }
}

?>