<?php
/**
 * @framework 3wymiar.pl 3.0 2002-2013 (c)
 *
 * @author Marcin Cichy <cichy@3wymiar.pl>
 * @version 1.0
 */

class Post_parser
{
    function __construct()
    {
        $this->allowed_tags = '';
    }

    function parse($html)
    {
        $html = stripslashes($html);
        $html = strip_tags($html, $this->allowed_tags);
        $html = $this->stripArgumentFromTags('<div>' . $html . '</div>');
        return $html;
    }

    function stripArgumentFromTags($htmlString)
    {
        $regEx = '/([^<]*<\s*[a-z](?:[0-9]|[a-z]{0,9}))(?:(?:\s*[a-z\-]{2,14}\s*=\s*(?:"[^"]*"|\'[^\']*\'))*)(\s*\/?>[^<]*)/i'; // match any start tag

        $chunks = preg_split($regEx, $htmlString, -1, PREG_SPLIT_DELIM_CAPTURE);
        $chunkCount = count($chunks);

        $strippedString = '';
        for ($n = 1; $n < $chunkCount; $n++) {
            $strippedString .= $chunks[$n];
        }
        return $strippedString;
    }
}