<?php
/**
 * @framework 3wymiar.pl 3.0 2002-2013 (c)
 *
 * @author Marcin Cichy <cichy@3wymiar.pl>
 * @version 1.0
 */

class Template
{
    function __construct($template_file, $from = '')
    {
        if (!file_exists($from . 'templates/' . $template_file . '.php')) {
            echo 'brak pliku template ' . $template_file;
            exit();
        }
        $this->from = $from;
        $this->head = '';
        $this->body = '';
        $this->template_file = $template_file;
    }

    function content_html()
    {
        global $site;
        require_once($this->from . 'templates/' . $this->template_file . '.php');
        return $this->head . $this->body . '</html>';
    }
}