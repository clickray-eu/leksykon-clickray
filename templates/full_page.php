<?php /** * @framework 3wymiar.pl 3.0 2002-2013 (c) * * @author Marcin Cichy <cichy@3wymiar.pl> * @version 1.0 */ /* * lista plików css i js używanych w głównym szablonie */
$site->css_list = array_merge($site->css_list, array('bootstrap.min', 'global', 'template_full_page'));
$site->js_list = array_merge($site->js_list, array('jquery-1.8.3.min', 'jquery-ui-1.9.2.custom.min', 'ui/minified/jquery.ui.core.min', 'bootstrap.min', 'global', 'searchbox')); /* * sekcja

<body>
    * zwracamy ją jako pierwszą na wypadek edycji head poprzez wgrywane pliki */
global $letters;
global $categoryArray;
$site_content = $site->load_site();
$this->body = '

    <body>
    <div id="main_wrapper">
            <div id="site_header_box">
                <div id="site_header">' . $site->load_module('site_header') . '</div>
            </div>
            <div id="site_mainframe">
                ' . $site->load_module('site_alerts') . '
                <div id="site_content">


                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12 col-md-9">
                                <h1>SŁOWNIK POJEĆ</h1>
                                <p>&nbsp;</p>
                                <ul id="list_letters">';

                                    if (empty($_GET['l'])) $_GET = array_push($_GET, 'l');

                                    $i = 0;
                                    foreach ($letters->list_letters() AS $letter) {
                                        $i++;
                                        $className = (($i == 1 && empty($_GET['l'])) || (mb_strtolower($letter, 'UTF-8') == $_GET['l'])) ? 'active' : '';
                                        $this->body .= '<li class="' . $className . 'linklike"><a href="http://10.0.0.92/leksykon-clickray/' . mb_strtolower($letter, 'UTF-8') . '/">' . $letter . '</a></li>';
                                        if ($letter == "M") $this->body .= "";
                                    }
                                    $this->body .= '

                                </ul>
                                <p class="al_clear">&nbsp;</p>
                                <div class="container-fluid nopadding">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6">
                                            <div id="search_box" class="home_search">
                                                Szukaj
                                                <p>
                                                    <input type="text" class="home_input search_input" placeholder="wpisz nazwę" />
                                                </p>
                                                <i class="icon_search fa fa-search" aria-hidden="true"></i>
                                                <div id="search_list" style="display:none">trwa wyszukiwanie, proszę czekać...</div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6">
                                            <div id="category_box" class="home_search">
                                                Wybierz kategorię
                                                <p class="home_input category_input showhide_category">wszystkie</p>
                                                <i class="icon_category showhide_category fa fa-arrow-circle-down" aria-hidden="true"></i>
                                                <div id="category_list" style="display:none">';
                                                    if (!empty($categoryArray)) {
                                                        $this->body .= '<ul>';
                                                        foreach ($categoryArray AS $cat) {
                                                            $this->body .= '
                                                            <li class="linklike">
                                                                <input type="checkbox" checked="checked" class="cat_checkbox al_left" value="' . $cat->id . '" />
                                                                <label class="checkboxlike al_left">' . stripslashes($cat->name) . '</label>
                                                                <div class="al_clear"></div>
                                                            </li>';
                                                        }
                                                        $this->body .= '</ul>';
                                                    }
                                                    $this->body .= '
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <div class="al_clear"></div>
                            <br>
                            <br> ' . $site_content . '
                            </div>
                            <div class="col-sm-12 col-md-3">
                            <aside class="sidebar">
                                <!--HubSpot Call-to-Action Code -->
                                <span class="hs-cta-wrapper" id="hs-cta-wrapper-c0ce1d0a-9640-43c1-b1ed-e48ef1071042">
                                    <span class="hs-cta-node hs-cta-c0ce1d0a-9640-43c1-b1ed-e48ef1071042" id="hs-cta-c0ce1d0a-9640-43c1-b1ed-e48ef1071042">
                                        <!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]-->
                                        <a href="http://clickray.pl/go/inbound-marketing-w-praktyce/"  target="_blank" ><img class="hs-cta-img" id="hs-cta-img-c0ce1d0a-9640-43c1-b1ed-e48ef1071042" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/685080/c0ce1d0a-9640-43c1-b1ed-e48ef1071042.png"  alt="New Call-to-action"/></a>
                                    </span>
                                </span>
                                <!-- end HubSpot Call-to-Action Code -->
                                    <div class="margin-top"></div>
                                
                                <!--HubSpot Call-to-Action Code -->
                                <span class="hs-cta-wrapper" id="hs-cta-wrapper-294dbb4d-58c5-46ca-a7ee-8ebdd039f71f">
                                    <span class="hs-cta-node hs-cta-294dbb4d-58c5-46ca-a7ee-8ebdd039f71f" id="hs-cta-294dbb4d-58c5-46ca-a7ee-8ebdd039f71f">
                                        <!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]-->
                                        <a href="http://clickray.pl/go/lekcja-generowania-leadow/"  target="_blank" ><img class="hs-cta-img" id="hs-cta-img-294dbb4d-58c5-46ca-a7ee-8ebdd039f71f" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/685080/294dbb4d-58c5-46ca-a7ee-8ebdd039f71f.png"  alt="New Call-to-action"/></a>
                                    </span>
                                </span>
                                <!-- end HubSpot Call-to-Action Code -->
                                
                                 <div class="margin-top"></div>
                                
                                <!--HubSpot Call-to-Action Code -->
                                <span class="hs-cta-wrapper" id="hs-cta-wrapper-cd56f4cd-33c8-4aff-bcfe-cd86795585fd">
                                    <span class="hs-cta-node hs-cta-cd56f4cd-33c8-4aff-bcfe-cd86795585fd" id="hs-cta-cd56f4cd-33c8-4aff-bcfe-cd86795585fd">
                                        <!--[if lte IE 8]><div id="hs-cta-ie-element"></div><![endif]-->
                                        <a href="http://clickray.pl/go/content-marketing-czyli-jak-stworzyc-powalajace-tresci/"  target="_blank" ><img class="hs-cta-img" id="hs-cta-img-cd56f4cd-33c8-4aff-bcfe-cd86795585fd" style="border-width:0px;" src="https://no-cache.hubspot.com/cta/default/685080/cd56f4cd-33c8-4aff-bcfe-cd86795585fd.png"  alt="New Call-to-action"/></a>
                                    </span>
                                </span>
                                <!-- end HubSpot Call-to-Action Code -->

                            </aside>
                            </div>
                        </div>
                    </div>
                    <div class="al_clear"></div>
                </div>
            </div>

          

            <div id="site_footer_box">
                <div id="site_footer">' . $site->load_module('site_footer') . '</div>
            </div>
              </div>

    </body>
    '; /* * sekcja

    <head>
        */
$this->head = '
        <!DOCTYPE html>
        <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="' . $site->service_config->lang . '" lang="' . $site->service_config->lang . '">

        <head>
            <title>' . $site->get_site_title() . '</title>
            <meta property="og:title" content="' . $site->get_site_title() . '" />
            <meta property="og:image" content="http://10.0.0.92/leksykon-clickray/public/gfx/home/sowa.png" />
            <meta charset="' . $site->service_config->charset . '" />
            <meta name="description" content="' . $site->site_description . '" />
            <meta name="keywords" content="' . $site->site_keywords . '" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta name="google-site-verification" content="uR631DIFCaPUVI43PNVMLXLLA7K8W1pKeRzHOye5EkQ" />
            <meta http-equiv="Content-Language" content="pl" />
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
            <!-- Latest compiled and minified CSS -->
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

            <!-- Optional theme -->
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
            <link href="https://fonts.googleapis.com/css?family=Palanquin:400,300,200,100,500,600,700&subset=latin,latin-ext" rel="stylesheet" type="text/css">

            <link href="http://' . $site->server_config->public_dir->host . '/' . $site->server_config->public_dir->gfx . 'favicon.ico" rel="SHORTCUT ICON" /> ' . $site->make_css_list() . ' ' . $site->make_js_list() . ' ' . $site->add_to_head . '
            <base href="http://' . $site->server_config->public_dir->host . '/" />
        </head>
        '; ?>
