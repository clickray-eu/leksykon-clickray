<?php
/**
 * @framework 3wymiar.pl 3.0 2002-2013 (c)
 *
 * @author Marcin Cichy <cichy@3wymiar.pl>
 * @version 1.0
 */
?>
<div class="container">
    <div class="row">
        <div class="col-lg-4 col-sm-6 col-xs-12">&copy; 2012 - <?php echo date("Y"); ?> Clickray Sp. z o.o.</div>
        <div class="col-lg-4 col-sm-6 col-xs-12 right-sm-left">
            <div class="right-sm-left">
                <a href="http://clickray.eu/pl/regulamin">Regulamin</a> | <a href="http://clickray.eu/pl/polityka-prywatnosci">Polityka Prywatności</a>
            </div>
        </div>
    </div>

</div>


<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-54668823-1', 'auto');
    ga('send', 'pageview');

</script>

<!-- Start of Async HubSpot Analytics Code -->
<script type="text/javascript">
    (function (d, s, i, r) {
        if (d.getElementById(i)) {
            return;
        }
        var n = d.createElement(s), e = d.getElementsByTagName(s)[0];
        n.id = i;
        n.src = '//js.hs-analytics.net/analytics/' + (Math.ceil(new Date() / r) * r) + '/685080.js';
        e.parentNode.insertBefore(n, e);
    })(document, "script", "hs-analytics", 300000);
</script>
<!-- End of Async HubSpot Analytics Code -->
