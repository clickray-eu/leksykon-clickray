<?php
/**
 * @framework 3wymiar.pl 3.0 2002-2013 (c)
 *
 * @author Marcin Cichy <cichy@3wymiar.pl>
 * @version 1.0
 */

$config = new Config();

if (!empty($_GET['q']) && !empty($_GET['c'])) {
    $SQLoption = $sql->get_results("SELECT id, value FROM 3w_options ORDER BY id ASC");
    $json = json_decode($SQLoption[4]->value);

    $exp = explode(' ', $_GET['q']);
    $where = '';
    foreach ($exp AS $sr) {
        if ($json->selected == 1) {
            $where .= "name LIKE '%" . mysqli_escape_string(mysqli_connect($config->get_sql_config()->host, $config->get_sql_config()->user, $config->get_sql_config()->password, $config->get_sql_config()->database), $sr) . "%' AND ";
        } elseif ($json->selected == 2) {
            $where .= "(name LIKE '%" . mysqli_escape_string(mysqli_connect($config->get_sql_config()->host, $config->get_sql_config()->user, $config->get_sql_config()->password, $config->get_sql_config()->database), $sr) . "%' OR clear_text LIKE '%" . mysqli_escape_string(mysqli_connect($config->get_sql_config()->host, $config->get_sql_config()->user, $config->get_sql_config()->password, $config->get_sql_config()->database), $sr) . "%') AND ";
        }
    }

    $where .= "(";
    $exp = explode(',', $_GET['c']);
    foreach ($exp AS $sr) {
        $where .= " category_id = '" . intval($sr) . "' OR ";
    }
    $where = substr($where, 0, -4) . ")";

    $fullquery = $sql->get_results("
  SELECT id, name, url 
  FROM 3w_dictionary_words
  WHERE " . $where . " AND deleted_at = '0'
  ORDER BY name ASC
  ");

    $firstletter = substr($_GET['q'], 0, 1);
    foreach ($fullquery AS $f) {
        $fl = substr($f->name, 0, 1);
        if (strtolower($fl) == strtolower($firstletter)) {
            $query[] = $f;
        }
    }
    foreach ($fullquery AS $f) {
        $fl = substr($f->name, 0, 1);
        if (strtolower($fl) != strtolower($firstletter)) {
            $query[] = $f;
        }
    }
}
?>