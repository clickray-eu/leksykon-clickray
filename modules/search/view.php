<?php
/**
 * @framework 3wymiar.pl 3.0 2002-2013 (c)
 *
 * @author Marcin Cichy <cichy@3wymiar.pl>
 * @version 1.0
 */

if (count($query) > 9) {
    echo 'Znaleziono ' . count($query) . ' pasujących wyników.<p>Zawęź wyniki określając frazy.</p>';
} elseif (count($query) > 0) {
    echo '<ul>';
    foreach ($query AS $var) {
        echo '<li class="linklike"><a href="http://10.0.0.92/leksykon-clickray/' . mb_strtolower(stripslashes(substr($var->name, 0, 1)), 'UTF-8') . '/' . $var->url . '">' . stripslashes($var->name) . '</a></li>';
    }
    echo '</ul>';
} else {
    echo 'nie znaleziono pasujących wyników';
}
?>