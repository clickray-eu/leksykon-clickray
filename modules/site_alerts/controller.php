<?php
/**
 * @framework 3wymiar.pl 3.0 2002-2013 (c)
 *
 * @author Marcin Cichy <cichy@3wymiar.pl>
 * @version 1.0
 */

global $alerts;
if (!empty($_SESSION['alert'])) {
    $display = '';
    $current_alert_class = $_SESSION['alert']->type;
    $alert_text = $_SESSION['alert']->text;
    $_SESSION['alert'] = '';
} elseif (!empty($alerts->text)) {
    $display = '';
    $current_alert_class = $alerts->type;
    $alert_text = $alerts->text;
} else {
    $display = 'style="display:none"';
}
?>