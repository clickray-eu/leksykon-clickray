<?php
/**
 * @framework 3wymiar.pl 3.0 2002-2013 (c)
 *
 * @author Marcin Cichy <cichy@3wymiar.pl>
 * @version 1.0
 */
$html = '
<div class="container">
<div class="row">
<div class="col-lg-4 col-sm-6 col-xs-12">

<div id="site_logo"><a href="http://clickray.pl/"><img src="' . GFX_URL . 'logo.png" alt="" /></a></div>

	</div>





<div id="module_plugins">
<div class="col-lg-4 col-sm-6 col-xs-12 right-sm-left">
<div class="right-sm-left">
<ul>
	<li><a href="https://www.facebook.com/inboundmarketingpolska/"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
	<li><a href="https://twitter.com/Clickray_PL"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
	<li><a href="https://www.linkedin.com/company/clickray"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a></li>
	<li><a href="https://plus.google.com/111260858427981750342/posts"><i class="fa fa-google-plus-square" aria-hidden="true"></i></a></li>
</ul>
	</div>
		</div>

</div>
	<div class="clearfix"></div>
	</div>
	</div>
';

if (!empty($headerArray)) {
    $html .= '<ul id="header_menu" class="al_right">';
    $i = 0;
    foreach ($headerArray AS $var) {
        $i++;
        $html .= '<li><a href="' . $var->url . '.html">' . $var->site_title . '</a></li>';
        $html .= ($i < count($headerArray)) ? '<li class="separator">|</li>' : '';
    }
    $html .= '</ul>';
}

echo stripslashes($html);
?>