<?php
/**
 * @framework 3wymiar.pl 3.0 2002-2013 (c)
 *
 * @author Marcin Cichy <cichy@3wymiar.pl>
 * @version 1.0
 */

//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

/*
* startujemy wszystkie sesje
*/
session_start();

/*
* robimy autolader dla class które wgrywamy w całym serwisie.
*/
function __autoload($class_name)
{
    if (file_exists('../libs/class.' . $class_name . '.php')) {
        require_once('../libs/class.' . $class_name . '.php');
    } else {
        echo 'brak klasy ' . $class_name;
        exit();
    }
}

/*
* wczytywanie ustawień serwisu
*/
$config = new Config();

/*
* czyścimy każdy get i post z tagów html
*/

$_GET = array_map("strip_tags", $_GET);
//if ($_SESSION['user']->admin != 1) {
// //   $_POST = array_map("strip_tags", $_POST);
//}

/*
* uruchamia serwis sql
*/
$sql = new Datebase_MySql($config->get_sql_config());

if (empty($_SERVER['PHP_AUTH_USER']) || empty($_SERVER['PHP_AUTH_PW']) ||
    $sql->get_var("
        SELECT COUNT(id)
        FROM 3w_auth 
        WHERE id = '1'
        AND login = '" . mysqli_escape_string(mysqli_connect($config->get_sql_config()->host, $config->get_sql_config()->user, $config->get_sql_config()->password, $config->get_sql_config()->database), $_SERVER['PHP_AUTH_USER']) . "'
        AND password = '" . mysqli_escape_string(mysqli_connect($config->get_sql_config()->host, $config->get_sql_config()->user, $config->get_sql_config()->password, $config->get_sql_config()->database), $_SERVER['PHP_AUTH_PW']) . "'") == 0) {

    Header('WWW-Authenticate: Basic realm="' . $config->get_service_config()->global_title . ' - panel administracyjny"');
    Header('HTTP/1.0 401 Unauthorized');
    echo 'Brak uprawnien do przegladania strony';
    exit;
}

/*
* site jako element cała strona na końcu zwracana jako html
*/
$site = new Site_builder('admin');

/*
* uruchamia serwis komuniaktów
*/
$alerts = new Alerts();

/*
* definiowanie ustawień wyświetlanego serwisu
*/
$service_config = $config->get_service_config();
$site->server_config = $config->get_server_config();
$site->service_config = $service_config;
$service_config->template_file = 'admin';
define('GFX_URL', $site->server_config->public_dir->gfx);

/*
* wgrywanie opcji z SQL
*/
if ($site->service_config->load_options_from_sql == true) {
    $SQLoption = $sql->get_results("SELECT id, value FROM 3w_options ORDER BY id ASC");
    if (!empty($SQLoption)) {
        $site->server_config->public_dir->host = $SQLoption[1]->value;
        $site->service_config->global_title = $SQLoption[2]->value;
        $site->service_config->global_separator = $SQLoption[3]->value;
    }
}

/*
* zwrot serwisu
*/
echo $site->content_html($_GET['go']);
?>