<?php
/**
 * @framework 3wymiar.pl 3.0 2002-2013 (c)
 *
 * @author Marcin Cichy <cichy@3wymiar.pl>
 * @version 1.0
 */

/*
 * lista plików css i js używanych w głównym szablonie
*/
$site->css_list = array_merge($site->css_list, array(
    'bootstrap.min',
    'global',
    'template_admin'
));
$site->js_list = array_merge($site->js_list, array(
    'jquery-1.8.3.min',
    'jquery-ui-1.9.2.custom.min',
    'ui/minified/jquery.ui.core.min',
    'bootstrap.min',
    'global',
    'admin'
));

/*
 * sekcja <body>
 * zwracamy ją jako pierwszą na wypadek edycji head poprzez wgrywane pliki
*/
$site_content = $site->load_site();
$this->body = '
<body>
  <div id="siteHTML">
	 <div id="admin_header">
	   ' . $site->load_module('admin_headmenu') . '
		' . $site->load_module('admin_breadcrumbs') . '
	 </div>
    <div id="admin_mainframe" class="al_clear">
	   ' . $site->load_module('admin_alerts') . '
      <div id="admin_content">' . $site_content . '</div>
	 </div>
	 <div id="admin_footer" class="al_clear">
	   ' . $site->load_module('admin_footer') . '
	 </div>
  </div>
</body>
';

/*
 * sekcja <head>
*/

$this->head = '
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="' . $site->service_config->lang . '" lang="' . $site->service_config->lang . '">
<head>
  <title>' . $site->get_site_title() . '</title>
  <meta charset="' . $site->service_config->charset . '" />
  <meta name="description" content="' . $site->site_description . '" />
  <meta name="keywords" content="' . $site->site_keywords . '" />
  <meta name="Author" content="Marcin Cichy" />
  <meta http-equiv="Reply-to" content="cichy@3wymiar.pl" />
  <meta http-equiv="Content-Language" content="pl" />
  <link href="http://' . $site->server_config->public_dir->host . '/admin/' . $site->server_config->public_dir->gfx . 'favicon.ico" rel="SHORTCUT ICON" /> 
  ' . $site->make_css_list() . '
  ' . $site->make_js_list() . '
  ' . $site->add_to_head . '
</head>
';
?>