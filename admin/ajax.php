<?php
/**
 * @framework 3wymiar.pl 3.0 2002-2013 (c)
 *
 * @author Marcin Cichy <cichy@3wymiar.pl>
 * @version 1.0
 */

header("Cache-Control: no-cache, must-revalidate");
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Content-type: text/html; charset=utf-8");

session_start();

/*
 * czyścimy każdy get i post z tagów html
*/
$_GET = array_map("strip_tags", $_GET);
if ($_SESSION['user']->admin != 1) {
    $_POST = array_map("strip_tags", $_POST);
}

/*
 * robimy autolader dla class które wgrywamy w całym serwisie.
*/
function __autoload($class_name)
{
    if (file_exists('../libs/class.' . $class_name . '.php')) {
        require_once('../libs/class.' . $class_name . '.php');
    } else {
        echo 'brak klasy ' . $class_name;
        exit();
    }
}

/*
 * wczytywanie ustawień serwisu
*/
$config = new Config();

/*
 * uruchamia serwis sql
*/
$sql = new Datebase_MySql($config->get_sql_config());

/*
 * site jako element cała strona na końcu zwracana jako html
*/
$site = new Site_builder('admin');

/*
 * uruchamia serwis komuniaktów
*/
$alerts = new Alerts();

/*
 * definiowanie ustawień wyświetlanego serwisu
 */
$service_config = $config->get_service_config();
$site->server_config = $config->get_server_config();
$site->service_config = $service_config;
$service_config->template_file = 'clear_ajax';
define('GFX_URL', $config->get_server_config()->public_dir->gfx);

/*if(empty($_SERVER['PHP_AUTH_USER']) ||  empty($_SERVER['PHP_AUTH_PW'])){
  echo '<div class="alert alert-error">dostęp tylko dla zalogowanych.</div>';
  exit();
}*/
/*
 * zwrot serwisu
*/
if (!empty($_GET['go'])) {
    echo $site->content_html($_GET['go']);
} elseif (!empty($_GET['module'])) {
    echo $site->load_module($_GET['module']);
}
?>