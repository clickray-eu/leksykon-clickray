/**
 * @framework 3wymiar.pl 3.0 2002-2013 (c)
 *
 * @author Marcin Cichy <cichy@3wymiar.pl>
 * @version 1.0
 */

$(document).ready(function(){
  sort_position_ready();
});

function sort_position_ready(){
  $('#sort_records').sortable({
    stop: function(){check_sort_moved(true);}
  });
}

function check_sort_moved(is_moved){
  var i=0;
  $('#sort_records li input').each(function(){
	 i++;
	 $(this).val(i);
  });
  if(is_moved == true){
    $('#save_positions').show();
  }
}