/**
 * @framework 3wymiar.pl 3.0 2002-2013 (c)
 *
 * @author Marcin Cichy <cichy@3wymiar.pl>
 * @version 1.0
 */

$(document).ready(function(){
  admin_words_ready();
});

function admin_words_ready(){
  $('#name').parent().append('<div id="resultbox"></div>');
  $('#name').keyup(function(){
	 $.ajax({
      url: 'ajax.php?module=check_word_name&name='+$('#name').val()+'&action='+$_GET['action']+'&id='+$_GET['id'],
	   type: 'GET',
      success: function(odp){
		  var json = jQuery.parseJSON(odp);
		  $('#name').parent().removeClass('alert-info');
		  $('#name').parent().removeClass('alert-success');
		  $('#name').parent().removeClass('alert-error');
		  $('#name').parent().addClass('alert-'+json.result);
		  $('#resultbox').html(json.text);
      }
    });
  });
}