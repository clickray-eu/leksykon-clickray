/**
 * @framework 3wymiar.pl 3.0 2002-2013 (c)
 *
 * @author Marcin Cichy <cichy@3wymiar.pl>
 * @version 1.0
 */

function after_upload(text, id, file, position){
  $('#status').removeAttr('class');
  $('#status').attr('class','alert alert-success');
  $('#status').html(text);
  $('#manage_gallery').append('<li class="al_left photo_gal"><span class="delete_photo btn btn-danger" id="del_'+id+'"><i class="icon-white icon-trash"></i></span><img src="http://www.amtenis.pl/galeria/'+file+'" class="img-polaroid" alt="" /> <input type="hidden" name="pos_'+id+'" value="'+position+'"></li>');
  delete_photo();
}

function after_upload_error(text){
  $('#status').removeAttr('class');
  $('#status').attr('class','alert alert-error');
  $('#status').html(text);
}