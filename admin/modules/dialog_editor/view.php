<?php
/**
 * @framework 3wymiar.pl 3.0 2002-2013 (c)
 *
 * @author Marcin Cichy <cichy@3wymiar.pl>
 * @version 1.0
 */

$html = '
  <ul class="nav nav-tabs dialog_editor_nav">
    <li ' . $class['img'] . '><a href="#" class="img" title="wstaw obrazek"><i class="icon-picture"></i> wstaw obrazek</a></li>
    <li ' . $class['movie'] . '><a href="#" class="movie" title="wstaw film z YouTube"><i class="icon-film"></i> wstaw film</a></a></li>
    <li ' . $class['file'] . '><a href="#" class="file" title="wstaw plik do pobrania"><i class="icon-download"></i> wstaw plik</a></a></li>
  </ul>

  <div id="editor_content" class="al_left">';

if ($_GET['action'] == 'img') {
    $html .= $claer_img_view;
} elseif ($_GET['action'] == 'movie') {

} elseif ($_GET['action'] == 'file') {
    $html .= $claer_file_view;
}

$html .= '
  </div>
  <div id="upload_content" class="al_right">
';

if ($_GET['action'] == 'img') {
    $html .= '
	   <form enctype="multipart/form-data" id="upload_content_form">
	   <strong>Wgraj plik</strong>
		<br />
		<div class="row2"><input type="file" name="img" id="upload_img" accept="image/*" /></div>
		<div class="row2"><span id="begin_upload_img" class="btn btn-success"><i class="icon-white icon-ok"></i></span></div>

		<div class="al_clear upload_box">
		  <input type="checkbox" name="make_thumbnail" id="make_thumbnail" value="1" class="upload_maker" checked="checked" /> <label for="make_thumbnail">Generuj miniaturkę</label>
		  <div class="upload_option_box" id="thumbnail_box">
		    <input type="radio" name="thumbnail_type" id="thumbnail_type_1" class="upload_type" value="1" checked="checked" /> <label for="thumbnail_type_1">kwadrat o boku</label> <input type="text" name="thumbnail_size" value="" placeholder="100" class="inp_int" />px
		    <br /><input type="radio" name="thumbnail_type" id="thumbnail_type_2" class="upload_type" value="2" /> <label for="thumbnail_type_2">prostokąt</label> <input type="text" name="thumbnail_width" value="" placeholder="" class="inp_int" />x<input type="text" name="thumbnail_height" value="" placeholder="" class="inp_int" />px
		  </div>
		</div>

		<div class="al_clear upload_box">
		  <input type="checkbox" name="make_scaled" id="make_scaled" value="1" class="upload_maker" checked="checked" /> <label for="make_scaled">Skaluj obrazek</label>
		  <div class="upload_option_box" id="scaled_box">
		    <input type="radio" name="scaled_type" id="scaled_type_1" class="upload_type" value="1" /> <label for="scaled_type_1">kwadrat o boku</label> <input type="text" name="scaled_size" placeholder="" value="" class="inp_int" />px
		    <br /><input type="radio" name="scaled_type" id="scaled_type_2" class="upload_type" value="2" checked="checked" /> <label for="scaled_type_2">prostokąt</label> <input type="text" name="scaled_width" value="" placeholder="640" class="inp_int" />x<input type="text" name="scaled_height" value="" placeholder="480" class="inp_int" />px
		  </div>
		</div>
		</form>
	 ';
} elseif ($_GET['action'] == 'movie') {
    $html .= '
	   <strong>Wskaż film</strong>
		<div class="alert alert-success">Wpisz samo ID filmu<p>www.youtube.com/watch?v=ID</p></div>
		<div class="row2"><input type="text" name="movie" id="upload_movie" placeholder="wskaż ID filmu" /></div>
		<div class="row2"><span id="begin_upload_movie" class="btn btn-success"><i class="icon-white icon-ok"></i></span></div>
		<div class="row3">
		  <div class="row1">Szerokość</div>
		  <div class="row2"><input type="text" id="movie_width" name="movie_width" value="" placeholder="640" class="inp_int" /> px</div>
		</div>
		<div class="row3">
		  <div class="row1">Wysokość</div>
		  <div class="row2"><input type="text" id="movie_height" name="movie_height" value="" placeholder="400" class="inp_int" /> px</div>
		</div>
	 ';
} elseif ($_GET['action'] == 'file') {
    $html .= '
	   <form enctype="multipart/form-data" id="upload_content_form">
	   <strong>Wgraj plik</strong>
		<br />
		<div class="row2"><input type="file" name="file" id="upload_file" /></div>
		<div class="row2"><span id="begin_upload_file" class="btn btn-success"><i class="icon-white icon-ok"></i></span></div>
		</form>
	 ';
}

$html .= '</div>
<div id="ajax_upload_dialog" style="display:none">
  Trwa wgrywanie pliku, proszę czekać...
  <p>&nbsp;</p>
  <div class="progress">
    <div class="bar bar-success" id="upload_progress" style="width: 1%;"></div>
  </div>
</div>';

echo $html;
?>