<?php
/**
 * @framework 3wymiar.pl 3.0 2002-2013 (c)
 *
 * @author Marcin Cichy <cichy@3wymiar.pl>
 * @version 1.0
 */
$upload = new Upload;
$gfx_upload_dir = 'www/img';
$file_upload_dir = 'www/file';

if ($_GET['action'] == 'delete' && !empty($_GET['file']) && !empty($_GET['ext']) && !empty($_GET['type'])) {
    if ($_GET['type'] == 'img') {
        $file = dirname(__FILE__) . '/../../../' . $gfx_upload_dir . '/' . str_replace('/', '_', $_GET['file']) . '.' . str_replace('/', '_', $_GET['ext']);
        $tfile = dirname(__FILE__) . '/../../../' . $gfx_upload_dir . '/' . str_replace('/', '_', $_GET['file']) . '_thumb.' . str_replace('/', '_', $_GET['ext']);
    } elseif ($_GET['type'] == 'file') {
        $file = dirname(__FILE__) . '/../../../' . $file_upload_dir . '/' . str_replace('/', '_', $_GET['file']) . '.' . str_replace('/', '_', $_GET['ext']);
    }

    if (file_exists($file)) {
        unlink($file);
    }
    if (file_exists($tfile)) {
        unlink($tfile);
    }
    exit();
}


$claer_img_view = '<p><input type="checkbox" checked="checked" id="add_polaroid"><label for="add_polaroid"> wstaw obrazek w ramce</label></p><hr />';

foreach (glob(dirname(__FILE__) . '/../../../' . $gfx_upload_dir . '/*') AS $filename) {
    $insert_cutter = str_replace('../../../', '|', $filename);
    $exp = explode('|', $insert_cutter);
    $address = $this->server_config->public_dir->host . '/' . $exp[1];

    $exp = explode('/', $address);
    $full_file = $exp[count($exp) - 1];

    $exp = explode('.', $full_file);
    $fexp = explode('_', $exp[0]);

    $imgArray[$fexp[0]]->type = $exp[1];

    if ($fexp[count($fexp) - 1] == 'thumb') {
        $imgArray[$fexp[0]]->thumbnail = $exp[0];
    } else {
        $imgArray[$fexp[0]]->file = $exp[0];
    }
}

foreach (glob(dirname(__FILE__) . '/../../../' . $file_upload_dir . '/*') AS $filename) {
    $insert_cutter = str_replace('../../../', '|', $filename);
    $exp = explode('|', $insert_cutter);
    $address = $this->server_config->public_dir->host . '/' . $exp[1];

    $exp = explode('/', $address);
    $full_file = $exp[count($exp) - 1];

    $exp = explode('.', $full_file);
    $fexp = explode('_', $exp[0]);

    $fileArray[$fexp[0]]->type = $exp[1];
    $fileArray[$fexp[0]]->file = $exp[0];
}

if (!empty($imgArray)) {
    foreach ($imgArray AS $img) {
        $info = getimagesize(dirname(__FILE__) . '/../../../' . $gfx_upload_dir . '/' . $img->file . '.' . $img->type);
        $claer_img_view .= '
	  <div class="img_insert_m al_left img-polaroid">
	    <div class="dropdown al_right">
	    <span class="btn btn-info dropdown-toggle" data-toggle="dropdown"><i class="icon-cog icon-white"></i><b class="caret"></b></span>
	    <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu">
	      <li class="nav-header">Informacje o obrazku</li>
	      <li class="editor">Wymiary: ' . $info[0] . 'x' . $info[1] . 'px</li>
			<li class="editor">Typ: ' . $img->type . '</li>
			<li class="editor">Waga: ' . $upload->filesize(dirname(__FILE__) . '/../../../' . $gfx_upload_dir . '/' . $img->file . '.' . $img->type) . '</li>';

        if (!empty($img->thumbnail)) {
            $tinfo = getimagesize(dirname(__FILE__) . '/../../../' . $gfx_upload_dir . '/' . $img->thumbnail . '.' . $img->type);
            $claer_img_view .= '<li class="divider"></li>
			  <li class="nav-header">Informacje o miniaturce</li>
	        <li class="editor">Wymiary: ' . $tinfo[0] . 'x' . $tinfo[1] . 'px</li>
			  <li class="editor">Waga: ' . $upload->filesize(dirname(__FILE__) . '/../../../' . $gfx_upload_dir . '/' . $img->thumbnail . '.' . $img->type) . '</li>
			  <li class="divider"></li>
	        <li><a href="#" class="insertThis" insert_type="thumbnail" insert_file="http://' . $this->server_config->public_dir->host . '/' . $gfx_upload_dir . '/' . $img->thumbnail . '.' . $img->type . '"><i class="icon-play-circle"></i> wstaw jako powiększalną miniaturę</a></li>';
        }

        $claer_img_view .= '<li class="divider"></li>
	      <li><a href="#" class="deleteFromEditor" title="skasuj obrazek" delete_file="' . $img->file . '|img|' . $img->type . '"><i class="icon-trash"></i> skasuj obrazek</a></li>
	    </ul>
		 </div>
	    <div class="img_insert insertThis linklike" title="kliknij by wstawić obrazek" insert_type="img" insert_file="http://' . $this->server_config->public_dir->host . '/' . $gfx_upload_dir . '/' . $img->file . '.' . $img->type . '" style="background: #fff url(\'http://' . $this->server_config->public_dir->host . '/' . $gfx_upload_dir . '/' . $img->file . '.' . $img->type . '\') no-repeat center center; background-size:contain;"></div>
	  </div>';
    }
} else {
    $claer_img_view .= '<div class="alert alert-warning">Brak wgranych plików.</div>';
}

if (!empty($fileArray)) {
    $claer_file_view = '
  <table class="table table-hover">
    <thead>
      <tr>
	     <th>Data dodania</th>
		  <th>Adres</th>
		  <th>Typ</th>
		  <th>Rozmiar</th>
		  <th>Opcje</th>
	   </tr>
    </thead>
    <tbody>';
    foreach ($fileArray AS $file) {
        $claer_file_view .= '<tr>
		    <td>' . date('d.m.Y H:i', filemtime(dirname(__FILE__) . '/../../../' . $file_upload_dir . '/' . $file->file . '.' . $file->type)) . '</td>
		    <td><a href="http://' . $this->server_config->public_dir->host . '/' . $file_upload_dir . '/' . $file->file . '.' . $file->type . '">' . $file_upload_dir . '/' . $file->file . '.' . $file->type . '</a></td>
		    <td>' . $file->type . '</td>
		    <td>' . $upload->filesize(dirname(__FILE__) . '/../../../' . $file_upload_dir . '/' . $file->file . '.' . $file->type) . '</td>
			 <td>
			   <span class="btn btn-success insertThis" insert_type="download" insert_file="http://' . $this->server_config->public_dir->host . '/' . $file_upload_dir . '/' . $file->file . '.' . $file->type . '"><i class="icon-white icon-ok"></i> wstaw</span>
			   <span class="btn btn-danger deleteFromEditor" title="skasuj plik" delete_file="' . $file->file . '|file|' . $file->type . '"><i class="icon-white icon-trash"></i></span>
			 </td>
		  </tr>';
    }
    $claer_file_view .= '</tbody>
  </table>
  ';
} else {
    $claer_file_view = '<div class="alert alert-warning">Brak wgranych plików.</div>';
}

if ($_POST['action'] == 'img') {
    $upload->upload_dir = $gfx_upload_dir;
    $thumbnail = json_decode(stripslashes($_POST['thumbnail']));
    $scale = json_decode(stripslashes($_POST['scaled']));
    if ($scale[0] == 1 || $thumbnail[0] == 1) {
        $upload->scale->make = ($scale[0] == 1) ? true : false;
        $upload->scale->type = $scale[1];
        $upload->scale->size = (intval($scale[2]) > 0) ? $scale[2] : 100;
        $upload->scale->width = (intval($scale[2]) > 0) ? $scale[2] : 640;
        $upload->scale->height = (intval($scale[3]) > 0) ? $scale[3] : 480;
        $upload->thumbnail->make = ($thumbnail[0] == 1) ? true : false;
        $upload->thumbnail->type = $thumbnail[1];
        $upload->thumbnail->size = (intval($thumbnail[2]) > 0) ? $thumbnail[2] : 100;
        $upload->thumbnail->width = (intval($thumbnail[2]) > 0) ? $thumbnail[2] : 640;
        $upload->thumbnail->height = (intval($thumbnail[3]) > 0) ? $thumbnail[3] : 480;
        $result = $upload->img_upload($_FILES['file']['tmp_name']);
    } else {
        $exp = explode('.', $_FILES['file']['name']);
        $upload->upload_file_extension = $exp[count($exp) - 1];
        $result = $upload->copy_file($_FILES['file']['tmp_name']);
    }

    if (!empty($result['ok'])) {
        echo 'ok';
    } else {
        echo 'error';
    }
    exit();
} elseif ($_POST['action'] == 'file') {
    $upload->upload_dir = $file_upload_dir;
    $exp = explode('.', $_FILES['file']['name']);
    $upload->upload_file_extension = $exp[count($exp) - 1];
    $upload->upload_file_name = $this->parse_name_for_url(str_replace('.' . $upload->upload_file_extension, '', $_FILES['file']['name']));
    $result = $upload->copy_file($_FILES['file']['tmp_name']);

    if (!empty($result['ok'])) {
        echo 'ok';
    } else {
        echo 'error';
    }
    exit();
} elseif ($_GET['action'] == 'clear_img') {
    $show_view = 'clear_img_view';
} elseif ($_GET['action'] == 'clear_file') {
    $show_view = 'clear_file_view';
} else {
    $class[$_GET['action']] = 'class="active"';
}
?>