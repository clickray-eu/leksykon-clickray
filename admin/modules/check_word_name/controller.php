<?php
/**
 * @framework 3wymiar.pl 3.0 2002-2013 (c)
 *
 * @author Marcin Cichy <cichy@3wymiar.pl>
 * @version 1.0
 */

$config = new Config();

if (empty($_GET['name'])) {
    echo '{"result":"error","text":"wpisz szukaną nazwę"}';
} else {
    if ($_GET['action'] == 'edit') {
        $duplicate = $sql->get_var("SELECT COUNT(id) FROM 3w_dictionary_words WHERE name = '" . mysqli_escape_string(mysqli_connect($config->get_sql_config()->host, $config->get_sql_config()->user, $config->get_sql_config()->password, $config->get_sql_config()->database), trim($_GET['name'])) . "' AND id != '" . intval($_GET['id']) . "' AND deleted_at = '0'");
    } else {
        $duplicate = $sql->get_var("SELECT COUNT(id) FROM 3w_dictionary_words WHERE name = '" . mysqli_escape_string(mysqli_connect($config->get_sql_config()->host, $config->get_sql_config()->user, $config->get_sql_config()->password, $config->get_sql_config()->database), trim($_GET['name'])) . "' AND deleted_at = '0'");
    }
    if ($duplicate == 0) {
        echo '{"result":"success","text":"<i class=icon-ok></i> wpisana nazwa jest dostępna"}';
    } else {
        echo '{"result":"error","text":"<h4>powtórzenie nazwy</h4>"}';
    }
}
?>