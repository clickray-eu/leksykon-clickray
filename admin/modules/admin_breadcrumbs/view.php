<?php
/**
 * @framework 3wymiar.pl 3.0 2002-2013 (c)
 *
 * @author Marcin Cichy <cichy@3wymiar.pl>
 * @version 1.0
 */

if (!empty($_GET['go'])) {
    $html = '<ul class="breadcrumb"><li><a href="http://' . $this->server_config->public_dir->host . '/admin"><i class="icon-home"></i></a> <span class="divider">' . $this->service_config->global_separator . '</span></li>';
    foreach ($admin_menu AS $parent_url => $menu) {
        if (!is_array($menu[2])) {
            if ($parent_url == $_GET['go']) {
                if (empty($_GET['action'])) {
                    $html .= '<li class="active">' . stripslashes($menu[0]) . '</li>';
                    $this->site_title[] = stripslashes($menu[0]);
                } else {
                    $html .= '<li><a href="index.php?go=' . $parent_url . '">' . stripslashes($menu[0]) . '</a> <span class="divider">' . $this->service_config->global_separator . '</span></li>';
                    $html .= '<li class="active">' . $action[$_GET['action']] . '</li>';
                    $this->site_title[] = stripslashes($menu[0]);
                    $this->site_title[] = stripslashes($action[$_GET['action']]);
                }
            }
        } else {
            foreach ($menu[2] AS $url => $submenu) {
                if ($url == $_GET['go']) {
                    $html .= '<li><a href="#">' . stripslashes($menu[0]) . '</a> <span class="divider">' . $this->service_config->global_separator . '</span></li>';
                    if (empty($_GET['action'])) {
                        $html .= '<li class="active">' . stripslashes($submenu[0]) . '</li>';
                        $this->site_title[] = stripslashes($menu[0]);
                        $this->site_title[] = stripslashes($submenu[0]);
                    } else {
                        $html .= '<li><a href="index.php?go=' . $url . '">' . stripslashes($submenu[0]) . '</a> <span class="divider">' . $this->service_config->global_separator . '</span></li>';
                        $html .= '<li class="active">' . $action[$_GET['action']] . '</li>';
                        $this->site_title[] = stripslashes($menu[0]);
                        $this->site_title[] = stripslashes($submenu[0]);
                        $this->site_title[] = stripslashes($action[$_GET['action']]);
                    }
                }
            }
        }
    }
    $html .= '</ul>';

    echo $html;
}
?>