<?php
/**
 * @framework 3wymiar.pl 3.0 2002-2013 (c)
 *
 * @author Marcin Cichy <cichy@3wymiar.pl>
 * @version 1.0
 */
global $admin_menu;
global $admin;

$action = array(
    'add' => 'dodaj ' . $admin->use_name,
    'edit' => 'zmień ' . $admin->use_name,
    'position' => 'ustaw ' . $admin->use_name,
    'gallery' => 'galeria ' . $admin->use_name . 'u',
    'models' => 'galeria modeli ' . $admin->use_name . 'u',
);
?>