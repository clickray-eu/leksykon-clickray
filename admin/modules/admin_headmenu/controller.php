<?php
/**
 * @framework 3wymiar.pl 3.0 2002-2013 (c)
 *
 * @author Marcin Cichy <cichy@3wymiar.pl>
 * @version 1.0
 */

global $admin_menu;

$admin_menu = array(
    'components' => array('Administracja', 'briefcase',
        array(
            'admin_options' => array('Główne ustawienia', 'cog'),
            'separator_1' => array(),
            'admin_components' => array('Zarządzaj komponentami', 'list-alt')
        )
    ),
    'dictionary' => array('Słownik', 'book',
        array(
            'admin_category' => array('Kategorię słów', 'tags'),
            'admin_words' => array('Słowa', 'tag'),
        )
    )
);

?>