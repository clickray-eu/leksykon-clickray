<?php
/**
 * @framework 3wymiar.pl 3.0 2002-2013 (c)
 *
 * @author Marcin Cichy <cichy@3wymiar.pl>
 * @version 1.0
 */

$html = '
<div class="navbar">
  <div class="navbar-inner">
    <a class="brand" href="http://' . $this->server_config->public_dir->host . '/admin">' . stripslashes($this->service_config->global_title) . ' admin</a>
    <ul class="nav">';
foreach ($admin_menu AS $parent_url => $menu) {
    $className = '';
    $html .= '<li class="divider-vertical"></li>';
    if (!is_array($menu[2])) {
        $html .= '<li class="' . $className . '"><a href="index.php?go=' . $parent_url . '"><i class="icon-' . $menu[1] . '"></i> ' . stripslashes($menu[0]) . '</a></li>';
    } else {
        $html2 = '
			   <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-' . $menu[1] . '"></i> ' . $menu[0] . ' <b class="caret"></b></a>
				<ul class="dropdown-menu">';
        foreach ($menu[2] AS $url => $submenu) {
            $exp = explode('_', $url);
            if ($exp[0] == 'separator') {
                $html2 .= '<li class="divider"></li>';
            } else {
                $subClassName = '';
                if ($_GET['go'] == $url) {
                    $subClassName = 'active';
                    $className = 'active';
                }
                $html2 .= '<li class="' . $subClassName . '"><a href="index.php?go=' . $url . '"><i class="icon-' . $submenu[1] . '"></i> ' . stripslashes($submenu[0]) . '</a></li>';
            }
        }
        $html2 .= '</ul>';
        $html .= '<li class="' . $className . ' dropdown">' . $html2 . '</li>';
    }
}
$html .= '<li class="divider-vertical"></li>';
$html .= '</ul>
  </div>
</div>';

echo $html;
?>