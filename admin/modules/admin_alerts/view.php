<?php
/**
 * @framework 3wymiar.pl 3.0 2002-2013 (c)
 *
 * @author Marcin Cichy <cichy@3wymiar.pl>
 * @version 1.0
 */

$html = '<div id="site_alerts" ' . $display . ' class="al_clear alert alert-' . $current_alert_class . '">' . $alert_text . '</div>';
echo $html;

?>