<?php
/**
 * @framework 3wymiar.pl 3.0 2002-2013 (c)
 *
 * @author Marcin Cichy <cichy@3wymiar.pl>
 * @version 1.0
 */
global $admin;

$admin = new SuperAdmin;
$form = new Form;
$config = new Config();

$this->css_list[] = 'ui-lightness/jquery-ui-1.9.1.custom';
$this->css_list[] = 'editor/jHtmlArea';
$this->css_list[] = 'editor/jHtmlArea.ColorPickerMenu';
$this->js_list[] = 'editor/jHtmlArea-0.7.0.min';
$this->js_list[] = 'editor/jHtmlArea.ColorPickerMenu-0.7.0.min';
$this->js_list[] = 'editor/editor';
$this->js_list[] = 'admin_words';

$admin->site_url = 'index.php?go=' . $_GET['go'];
$admin->table_name = '3w_dictionary_words';
$admin->table_sort = 'name';
$admin->use_name = 'słowo do słownika';
$admin->parent_list = $sql->get_results("SELECT id, name AS value FROM 3w_dictionary_category WHERE deleted_at = '0'");
$admin->show_search = true;
$admin->search_field = 'name';

$admin->table_headers = array(
    'słowo' => 'name',
    'opcje' => 'options'
);
$admin->insert = array(
    'url' => array('input', 'adres URL', 1, '', '<div class="alert alert-info">Pod jakim adresem będzie dostępne słowo', 150),
    'name' => array('input', 'słowo', 1, '', '<div class="alert alert-info">Nazwa używana w wyszukiwarkach i generalnie używana w serwisie', 250),
    'category_id' => array('select', 'kategoria', 1, $admin->parent_list),
    'body' => array('html', 'treść', 0, '', '<div class="alert alert-info">Opis wyświetlany po wejściu w dane słowo', 750, 500),
    'site_title' => array('input', 'tytuł strony', 1, '', '<div class="alert alert-warning">Tytuł wyświetlany w nagłówku przeglądarki z zastosowaniem dla lepszego indeksowania przez wyszukiwarki', 250),
    'site_keywords' => array('textarea', 'słowa kluczowe', 0, '', '<div class="alert alert-warning">Słowa wyświetlane w meta tagu na stronie słowa', 500, 50),
    'site_description' => array('textarea', 'opis strony', 0, '', '<div class="alert alert-warning">Opis wyświetlany w meta tagu na stronie słowa', 500, 100),
);

if (!empty($_POST['action'])) {
    $admin->insert['clear_text'] = array();
    $_POST['clear_text'] = strip_tags($_POST['body']);
    $_POST['url'] = $this->parse_name_for_url($_POST['url']);
    if ($_POST['action'] == 'add') {
        $duplicate = $sql->get_var("
            SELECT COUNT(id)
            FROM " . $admin->table_name . "
            WHERE name = '" . mysqli_escape_string(mysqli_connect($config->get_sql_config()->host, $config->get_sql_config()->user, $config->get_sql_config()->password, $config->get_sql_config()->database), trim($_POST['name'])) . "'
            AND deleted_at = '0'");

        if ($duplicate > 0) {
            $_SESSION['alert']->type = 'error';
            $_SESSION['alert']->text = 'powtórzenie słowa <strong>' . $_POST['name'] . '</strong>.<p>Słowo nie zostało dodane!</p>';
            header('location: ' . $admin->site_url);
            exit();
        }
        $html = $admin->add_new();
    } elseif ($_POST['action'] == 'edit') {
        $html = $admin->save_form();
    }
} elseif (!empty($_GET['action'])) {
    if ($_GET['action'] == 'edit' || $_GET['action'] == 'add') {
        $html = $admin->construct_form();
    } elseif ($_GET['action'] == 'delete') {
        $admin->delete_row();
        exit();
    }
} else {
    $html = $admin->show_table();
}
?>