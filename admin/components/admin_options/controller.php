<?php
/**
 * @framework 3wymiar.pl 3.0 2002-2013 (c)
 *
 * @author Marcin Cichy <cichy@3wymiar.pl>
 * @version 1.0
 */

$form = new Form;
$site_url = 'index.php?go=' . $_GET['go'];
$config = new Config();

$optionsArray = $sql->get_results("SELECT id, description, value, type FROM 3w_options ORDER BY id ASC");
foreach ($optionsArray AS $option) {
    if ($option->type == 'text') {
        $inp[$option->id] = new Input;
        $inp[$option->id]->width = 200;
    } elseif ($option->type == 'int') {
        $inp[$option->id] = new Restricted;
        $inp[$option->id]->width = 100;
        $inp[$option->id]->allow_int = 1;
    } elseif ($option->type == 'textarea') {
        $inp[$option->id] = new Textarea;
        $inp[$option->id]->width = 500;
        $inp[$option->id]->height = 60;
    } elseif ($option->type == 'select') {
        $json = json_decode($option->value);
        $inp[$option->id] = new Select;
        foreach ($json->elements AS $key => $element) {
            $inp[$option->id]->elements[$key] = $element;
        }
        $inp[$option->id]->selected = $json->selected;
    }

    if ($option->type == 'password') {
        $inp[$option->id] = new Password;
        $inp[$option->id]->width = 200;
        $inp[$option->id]->disabled = 1;
    } else {
        $inp[$option->id]->required = 1;
        $inp[$option->id]->value = $option->value;
    }

    $inp[$option->id]->name = 'option_' . $option->id;
    $inp[$option->id]->field_id = 'option_' . $option->id;
}

if ($_POST['action'] == 'save') {
    foreach ($_POST AS $key => $val) {
        $exp = explode('_', $key);

        if (!empty($optionsArray[$exp[1]])) {
            if ($optionsArray[$exp[1]]->type == 'select') {
                $nval = '';
                $nval->elements = $json->elements;
                $nval->selected = $val;
                $val = json_encode($nval);
            }
        }

        if ($exp[0] == 'option') {
            $sql->query("UPDATE 3w_options SET value = '" . mysqli_escape_string(mysqli_connect($config->get_sql_config()->host, $config->get_sql_config()->user, $config->get_sql_config()->password, $config->get_sql_config()->database), $val) . "' WHERE id = '" . intval($exp[1]) . "' ");
        }
    }
    $_SESSION['alert']->type = 'success';
    $_SESSION['alert']->text = 'zmiany zostały zapisane';
    header('location: ' . $site_url);
    exit();
} elseif ($_POST['action'] == 'changeadmin') {
    $old = $sql->get_row("SELECT login, email FROM 3w_auth WHERE id = '1'");
    $newpass = SuperAdmin::rand_string(rand(8, 12));

    if (!empty($_POST['adminlogin'])) {
        $newlogin = mysqli_escape_string(mysqli_connect($config->get_sql_config()->host, $config->get_sql_config()->user, $config->get_sql_config()->password, $config->get_sql_config()->database), $_POST['adminlogin']);
    } else {
        $newlogin = $old->login;
    }

    $sql->query("UPDATE 3w_auth SET login = '" . $newlogin . "', password = password('" . $newpass . "') WHERE id = '1'");

    $headers = "From: serwer@" . str_replace('www.', '', $optionsArray[1]->value) . "\r\n";
    $headers .= "Reply-To: serwer@" . str_replace('www.', '', $optionsArray[1]->value) . "\r\n";
    $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
    $title = $optionsArray[2]->value . ' nowe dane dostępowe';
    $body = "<a href=\"http://" . $optionsArray[1]->value . "/admin/\">http://" . $optionsArray[1]->value . "/admin/</a><br />Login: " . $newlogin . "<br />Hasło: " . $newpass;
    mail($old->email, $title, $body, $headers);

    $_SESSION['alert']->type = 'success';
    $_SESSION['alert']->text = 'dane dostępowe zostały zrestowane';
    header('location: ' . $site_url);
    exit();
}

?>