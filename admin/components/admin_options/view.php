<?php
/**
 * @framework 3wymiar.pl 3.0 2002-2013 (c)
 *
 * @author Marcin Cichy <cichy@3wymiar.pl>
 * @version 1.0
 */
$html = '
<div class="al_left">
<form action="' . $site_url . '" method="POST">
  <div class="alert alert-success">
    <h4>Globalne ustawienia</h4>
	 <p>Określ podstawowe dane serwisu.</p>
  </div>
  ';
foreach ($optionsArray AS $option) {
    $html .= '<div class="row3"><div class="row2"><div class="alert alert-info">' . stripslashes($option->description) . '</div><p>' . $inp[$option->id]->show() . '</p></div></div>';
}
$html .= '
  <div class="row1"></div><div class="row2"><button class="btn btn-success"><i class="icon-white icon-cog"></i> zapisz zmiany</button></div>
  <input type="hidden" name="action" value="save" />
</form>
</div>

<div class="al_right">
<form action="' . $site_url . '" method="POST">
  <div class="alert alert-warning">
    <h4>Zmiana danych dostępowych</h4>
	 <p>Wypełnij nowy login jeżeli chcesz zmienić.
	 <br />Zmianiając login zawsze resetujesz hasło.</p>
	 <p>Pozostaw puste jeżeli chcesz tylko zresetować hasło.</p>
	 <p>Nowe hasło zostanie wysłane na systemowy email.</p>
  </div>
  <div class="row1"><h3>Nowy login admina:</h3></div>
  <div class="row2"><input type="text" name="adminlogin" autocomplete="off" value="" /></div>
  <hr />
  <div class="row2"><button class="btn btn-danger"><i class="icon-white icon-cog"></i> zresetuj hasło admina</button></div>
  <input type="hidden" name="action" value="changeadmin" />
</form>
</div>
';

echo $html;
?>