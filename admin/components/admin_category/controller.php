<?php
/**
 * @framework 3wymiar.pl 3.0 2002-2013 (c)
 *
 * @author Marcin Cichy <cichy@3wymiar.pl>
 * @version 1.0
 */
global $admin;

$admin = new SuperAdmin;
$form = new Form;

$this->css_list[] = 'ui-lightness/jquery-ui-1.9.1.custom';
$this->css_list[] = 'editor/jHtmlArea';
$this->css_list[] = 'editor/jHtmlArea.ColorPickerMenu';
$this->js_list[] = 'editor/jHtmlArea-0.7.0.min';
$this->js_list[] = 'editor/jHtmlArea.ColorPickerMenu-0.7.0.min';
$this->js_list[] = 'editor/editor';

$admin->site_url = 'index.php?go=' . $_GET['go'];
$admin->table_name = '3w_dictionary_category';
$admin->use_name = 'kategorię';

$admin->table_headers = array(
    'nazwa kategorii' => 'site_title',
    'opcje' => 'options'
);
$admin->insert = array(
    'url' => array('input', 'adres URL', 1, '', '<div class="alert alert-info">Pod jakim adresem będzie dostępna kategoria', 150),
    'name' => array('input', 'nazwa kategorii', 1, '', '<div class="alert alert-info">Nazwa używana w wyszukiwarkach i generalnie używana w serwisie', 250),
    'body' => array('html', 'opis kategorii', 0, '', '<div class="alert alert-info">Opis wyświetlany po wejściu w kategorię', 750, 200),
    'site_title' => array('input', 'tytuł strony', 1, '', '<div class="alert alert-warning">Tytuł wyświetlany w nagłówku przeglądarki z zastosowaniem dla lepszego indeksowania przez wyszukiwarki', 250),
    'site_keywords' => array('textarea', 'słowa kluczowe', 0, '', '<div class="alert alert-warning">Słowa wyświetlane w meta tagu na stronie kategorii', 500, 50),
    'site_description' => array('textarea', 'opis strony', 0, '', '<div class="alert alert-warning">Opis wyświetlany w meta tagu na stronie kategorii', 500, 100),
);

if (!empty($_POST['action'])) {
    $_POST['url'] = $this->parse_name_for_url($_POST['url']);
    if ($_POST['action'] == 'add') {
        $html = $admin->add_new();
    } elseif ($_POST['action'] == 'edit') {
        $html = $admin->save_form();
    }
} elseif (!empty($_GET['action'])) {
    if ($_GET['action'] == 'edit' || $_GET['action'] == 'add') {
        $html = $admin->construct_form();
    } elseif ($_GET['action'] == 'delete') {
        $admin->delete_row();
        exit();
    }
} else {
    $html = $admin->show_table();
}
?>