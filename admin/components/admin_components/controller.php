<?php
/**
 * @framework 3wymiar.pl 3.0 2002-2013 (c)
 *
 * @author Marcin Cichy <cichy@3wymiar.pl>
 * @version 1.0
 */
global $admin;

$admin = new SuperAdmin;
$form = new Form;

$this->css_list[] = 'ui-lightness/jquery-ui-1.9.1.custom';
$this->css_list[] = 'editor/jHtmlArea';
$this->css_list[] = 'editor/jHtmlArea.ColorPickerMenu';
$this->js_list[] = 'editor/jHtmlArea-0.7.0.min';
$this->js_list[] = 'editor/jHtmlArea.ColorPickerMenu-0.7.0.min';
$this->js_list[] = 'editor/editor';

$admin->site_url = 'index.php?go=' . $_GET['go'];
$admin->table_name = '3w_components';
$admin->table_sort = 'position';
$admin->use_name = 'komponent';
$admin->change_position = true;
$admin->position_value = "site_title AS value";
$admin->parent_list = $sql->get_results("SELECT id, site_title AS value FROM " . $admin->table_name . " WHERE parent_id = '0' AND deleted_at = '0'");
$admin->parent_list[0] = 'Główny dział';

$admin->table_headers = array(
    'nazwa strony' => 'site_title',
    'adres strony' => 'url',
    'dział' => 'parent_id',
    'opcje' => 'options'
);
$admin->insert = array(
    'parent_id' => array('select', 'dział', 0, $admin->parent_list),
    'url' => array('input', 'adres URL', 1, '', '', 200),
    'site_title' => array('input', 'nazwa strony', 1, '', '', 250),
    'site_keywords' => array('textarea', 'słowa kluczowe', 0, '', '', 500, 50),
    'site_description' => array('textarea', 'opis strony', 0, '', '', 500, 100),
    'body' => array('html', 'treść strony', 0, '', '', 750, 500),
    'file' => array('input', 'plik PHP', 0, '', '<div class="alert alert-warning">wskaż nazwę pliku komponentu<p>Pozostaw puste jeżeli dodajesz stronę tekstową</p>', 200)
);

if ($_GET['action'] == 'edit') {
    if ($sql->get_var("SELECT file FROM " . $admin->table_name . " WHERE id = '" . intval($_GET['id']) . "'") != '') {
        unset($admin->insert['body']);
    }
}

if (!empty($_POST['action'])) {
    $_POST['url'] = $this->parse_name_for_url($_POST['url']);
    if ($_POST['action'] == 'add') {
        $html = $admin->add_new();
    } elseif ($_POST['action'] == 'edit') {
        $html = $admin->save_form();
    } elseif ($_POST['action'] == 'save_positions') {
        $html = $admin->save_elements_position();
    }
} elseif (!empty($_GET['action'])) {
    if ($_GET['action'] == 'edit' || $_GET['action'] == 'add') {
        $html = $admin->construct_form();
    } elseif ($_GET['action'] == 'position') {
        $html = $admin->change_elements_position();
    } elseif ($_GET['action'] == 'delete') {
        $admin->delete_row();
        exit();
    }
} else {
    $html = $admin->show_table();
}
?>