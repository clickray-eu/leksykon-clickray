<?php
/**
 * @framework 3wymiar.pl 3.0 2002-2013 (c)
 *
 * @author Marcin Cichy <cichy@3wymiar.pl>
 * @version 1.0
 */
if (empty($query)) {
    echo '<div class="alert alert-warning">brak słów zaczynających się na literę <strong>' . $_GET['l'] . '</strong>.</div>';
} else {
    foreach ($query AS $var) {
        echo '<h4 class="letter_link"><a href="http://10.0.0.92/leksykon-clickray/' . mb_strtolower(mb_substr(stripslashes($var->name), 0, 1), 'UTF-8') . '/' . $var->url . '">' . stripslashes($var->name) . '</a></h4>';

        if (strlen(stripslashes($var->clear_text)) < 200) {
            echo '<p>' . stripslashes($var->clear_text) . '</p>';
        } else {
            $fulltext = substr(stripslashes($var->clear_text), 0, 200);
            $text = explode('.', $fulltext);
            $i = 0;
            if (count($text) > 1) {
                echo '<p>';
                foreach ($text AS $t) {
                    $i++;
                    if ($i < count($text)) {
                        echo $t . '.';
                    }
                }
                echo '</p>';
            } else {
                echo '<p>' . substr(stripslashes($var->clear_text), 0, 200) . '...</p>';
            }
        }
        echo '<p><a href="http://10.0.0.92/leksykon-clickray/' . mb_strtolower(mb_substr(stripslashes($var->name), 0, 1), 'UTF-8') . '/' . $var->url . '" class="al_right">czytaj więcej <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>
              </a></p>';
        echo '<div class="al_clear"></div>';
        echo '<hr />';
    }
    if ($paginator->count_rows > $paginator->elements_on_page) {
        echo $paginator->content_html();
    }
}
?>