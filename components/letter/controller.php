<?php
/**
 * @framework 3wymiar.pl 3.0 2002-2013 (c)
 *
 * @author Marcin Cichy <cichy@3wymiar.pl>
 * @version 1.0
 */
$this->css_list[] = 'home';

$config = new Config();

$paginator = new Paginator;
$paginator->start = intval($_GET['start']);
$paginator->elements_on_page = 10;
$paginator->radius = 3;
$paginator->count_rows = $sql->get_var("SELECT COUNT(id) FROM 3w_dictionary_words WHERE name LIKE '" . mysqli_escape_string(mysqli_connect($config->get_sql_config()->host, $config->get_sql_config()->user, $config->get_sql_config()->password, $config->get_sql_config()->database), $_GET['l']) . "%' AND deleted_at = '0'");
$paginator->link_class; // klasa dla linków
$paginator->active_class = 'active';

$this->site_title[] = $_GET['l'];
$query = $sql->get_results("
    SELECT id, name, url, clear_text
    FROM 3w_dictionary_words
    WHERE name LIKE '" . mysqli_escape_string(mysqli_connect($config->get_sql_config()->host, $config->get_sql_config()->user, $config->get_sql_config()->password, $config->get_sql_config()->database), $_GET['l']) . "%' AND deleted_at = '0'
    ORDER BY name ASC
    LIMIT " . ($paginator->start * $paginator->elements_on_page) . ", " . $paginator->elements_on_page
);
?>