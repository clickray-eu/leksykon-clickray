<?php
/**
 * @framework 3wymiar.pl 3.0 2002-2013 (c)
 *
 * @author Marcin Cichy <cichy@3wymiar.pl>
 * @version 1.0
 */

$this->css_list[] = 'home';
$config = new Config();

$word = $sql->get_row("SELECT id, name, body, site_title, site_keywords, site_description
                                 FROM 3w_dictionary_words
                                 WHERE url = '" . mysqli_escape_string(mysqli_connect($config->get_sql_config()->host, $config->get_sql_config()->user, $config->get_sql_config()->password, $config->get_sql_config()->database), str_replace("-", "_", $_GET['word'])) . "' AND deleted_at = '0'
                                 ORDER BY name ASC");

$this->site_title[] = $_GET['l'];
$this->site_title[] = stripslashes($word->site_title);
$this->site_keywords = stripslashes($word->site_keywords);
$this->site_description = stripslashes($word->site_description);

?>