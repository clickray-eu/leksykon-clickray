<?php
/**
 * @framework 3wymiar.pl 3.0 2002-2013 (c)
 *
 * @author Marcin Cichy <cichy@3wymiar.pl>
 * @version 1.0
 */
?>
<div id="home_text">
    <img src="<?php echo GFX_URL; ?>home/sowa.png" alt="" class="al_right"/>
    <p>Jeżeli nie jest Ci obce pojęcie inbound marketingu, na pewno spotkałeś się z pewnymi terminami, które określają różne strategie, najlepsze techniki, dane czy pomiary. Słyszałeś prawdopodobnie o korzystaniu z # tagów na Twitterze, o tym jak
        ważne jest pozyskiwanie linków przychodzących lub w jaki sposób opracować tzw. drip campaign (dosłownie kampania kropelkowa), czyli spersonalizowaną kampanię e-mailingową, mającą na celu pielęgnowanie naszych leadów sprzedażowych. Co to
        wszystko znaczy? Czy miałeś kiedyś problemy z rozszyfrowaniem tego enigmatycznego, zawiłego żargonu? Nie obawiaj się! Specjalnie dla Ciebie przygotowaliśmy inbound marketingowy glosariusz, zawierający wszystkie najważniejsze pojęcia inbound
        marketingowe, które powinieneś znać. Niniejszy słownik, został podzielony na osobne kategorie, ze względu na poszczególne obszary inbound marketingu: pozyskiwanie leadów, strony docelowe, e-mail marketing, blogowanie i SEO, automatyzacja
        marketingu, marketing w mediach społecznościowych oraz marketing pod kątem urządzeń mobilnych. Jeżeli istnieją zatem pojęcia, których znaczenia, do tej pory, nie udało Ci się rozszyfrować, istnieją spore szanse, że znajdziesz je właśnie
        tutaj. Nie pozwól żeby nieznajomość terminologii powstrzymała Cię przed osiągnięciem sukcesu: zapoznaj się ze wszystkimi definicjami, jakie mogą kiedykolwiek okazać Ci się potrzebne i zostań gwiazdą inbound marketingu.</p>
</div>