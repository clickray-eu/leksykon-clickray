<?php
/**
 * @framework 3wymiar.pl 3.0 2002-2013 (c)
 *
 * @author Marcin Cichy <cichy@3wymiar.pl>
 * @version 1.0
 */

//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

/*
 * startujemy wszystkie sesje
*/
session_start();

/*
 * czyścimy każdy get i post z tagów html
*/

$_GET = array_map("strip_tags", $_GET);
if ($_SESSION['user']->admin != 1) {
    $_POST = array_map("strip_tags", $_POST);
}

/*
 * robimy autolader dla class które wgrywamy w całym serwisie.
*/
function __autoload($class_name)
{
    if (file_exists('libs/class.' . $class_name . '.php')) {
        require_once('libs/class.' . $class_name . '.php');
    } else {
        echo 'brak klasy ' . $class_name;
        exit();
    }
}

/*
 * wczytywanie ustawień serwisu
*/
$config = new Config();

/*
 * uruchamia serwis sql
*/
$sql = new Datebase_MySql($config->get_sql_config());

/*
 * site jako element cała strona na końcu zwracana jako html
*/
$site = new Site_builder();

/*
 * uruchamia serwis komuniaktów
*/
$alerts = new Alerts();

/*
 * definiowanie ustawień wyświetlanego serwisu
 */
$service_config = $config->get_service_config();
$site->server_config = $config->get_server_config();
$site->service_config = $service_config;
$service_config->template_file = 'full_page';
define('GFX_URL', 'http://' . $config->get_server_config()->public_dir->host . '/' . $config->get_server_config()->public_dir->gfx);
if (!empty($_GET['l']) && !isset($_GET['word'])) {
    $_GET['go'] = 'letter';
} elseif (!empty($_GET['l']) && !empty($_GET['word'])) {
    $_GET['go'] = 'word';
} else {
    $_GET['go'] = str_replace('/', '_', $_GET['go']);
    if (empty($_GET['go'])) {
        $_GET['go'] = 'home';
    }
}

/*
 * wgrywanie opcji z SQL
*/
if ($site->service_config->load_options_from_sql == true) {
    $SQLoption = $sql->get_results("SELECT id, value FROM 3w_options ORDER BY id ASC");
    if (!empty($SQLoption)) {
        $site->server_config->public_dir->host = $SQLoption[1]->value;
        $site->service_config->global_title = $SQLoption[2]->value;
        $site->service_config->global_separator = $SQLoption[3]->value;
    }
}

$letters = new Letters;
$categoryArray = $sql->get_results("SELECT id, url, name FROM 3w_dictionary_category WHERE deleted_at = '0' ORDER BY name ASC");

/*
 * zwrot serwisu
*/
echo $site->content_html($_GET['go']);
?>