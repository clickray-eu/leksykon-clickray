/**
 * @framework 3wymiar.pl 3.0 2002-2013 (c)
 *
 * @author Marcin Cichy <cichy@3wymiar.pl>
 * @version 1.0
 */

var watchmaker_interval = setInterval('watchmaker_ready()',1000*30);

$(document).ready(function(){
  watchmaker_ready();
});

function watchmaker_ready(){
  $('.watchmaker').each(function(){
    var time = parseInt($(this).attr('id'));
	 var a_time = Math.round(+new Date()/1000);
	 var minutes = Math.ceil((a_time-time)/60);
	 var minutes_string = minutes.toString();
	 var last_char = parseInt(minutes_string.substr(-1));
	 if(minutes < 2){
	   var return_nr =  translate.mniej_niz_minute;
	 }
	 else if(minutes < 5){
	   var return_nr =  minutes+' '+translate.minuty+' '+translate.temu;
	 }
	 else if(minutes < 22){
	   var return_nr =  minutes+' '+translate.minut+' '+translate.temu;
	 }
	 else if(last_char > 1 && last_char < 5){
	   var return_nr =  minutes+' '+translate.minuty+' '+translate.temu;
	 }
	 else{
	   var return_nr =  minutes+' '+translate.minut+' '+translate.temu;
	 }
	 if(minutes > 60){
	   var t = new Date();
		t.setTime(time);
	   $(this).html(translate.dzis_o+' '+t.getHours()+':'+t.getMinutes());
		$(this).removeClass('watchmaker');
	 }
	 else{
	   $(this).html(return_nr);
	 }
  });
  $('.watchmaker_future').each(function(){
    var time = parseInt($(this).attr('id'));
	 var a_time = Math.round(+new Date()/1000);
	 var minutes = Math.ceil((time-a_time)/60);
	 var minutes_string = minutes.toString();
	 var last_char = parseInt(minutes_string.substr(-1));
	 if(minutes < 2){
	   var return_nr =  translate.za_mniej_niz_minute;
	 }
	 else if(minutes < 5){
	   var return_nr =  translate.za+' '+minutes+' '+translate.minuty;
	 }
	 else if(minutes < 22){
	   var return_nr =  translate.za+' '+minutes+' '+translate.minut;
	 }
	 else if(last_char > 1 && last_char < 5){
	   var return_nr =  translate.za+' '+minutes+' '+translate.minuty;
	 }
	 else{
	   var return_nr =  translate.za+' '+minutes+' '+translate.minut;
	 }
	 if(minutes > 60){
	   var t = new Date();
		t.setTime(time);
	   $(this).html(translate.dzis_o+' '+t.getHours()+':'+t.getMinutes());
		$(this).removeClass('watchmaker');
	 }
	 else{
	   $(this).html(return_nr);
	 }
  });
}