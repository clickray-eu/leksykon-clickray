/**
 * @framework 3wymiar.pl 3.0 2002-2013 (c)
 *
 * @author Marcin Cichy <cichy@3wymiar.pl>
 * @version 1.0
 */

/* akcje dla wszystkich formularzy */
$(document).ready(function(){
  /*
   * sprawdzamy czy form ma nadane id
	* jak nie, nadajemy
	*/
  $('form').each(function(){
	  if($(this).attr('id') == undefined){
	    var form_id = 'form_'+Math.random();
		 $(this).attr('id',form_id.replace('0.',''));
	  }
	  //$(this).css({'position':'relative'});
	  all_forms_ready();
  });
  $('button').disableSelection();
  error_fields_ready();
  check_all_fields();
  ohter_form_action_ready();
});

function all_forms_ready(){
  /*
   * sprawdzanie czy wszystkie wymgane są wypełnion
	* przed puszczeniem SUBMIT
	*/
	$('form').unbind('submit');
	$('form').submit(function(){
	  var form_id = $(this).attr('id');
	  var required_past = true;
	  $('#'+form_id+' .required_field').each(function(){
	    if($(this).hasClass('dialogEditor')){
		   if($('#'+form_id+' .dialogEditor').htmlarea('toHtmlString') == '<br>'){
		     required_past = false;
			  $(this).parent().addClass('required_error');
			}
		   else if($('#'+form_id+' .dialogEditor').htmlarea('toHtmlString') != '<br>'){
		     required_past = true;
			  $(this).parent().removeClass('required_error');
			}
		 }
	    else if(($(this).attr('type') == 'radio' || $(this).attr('type') == 'checkbox') && $(this).attr('checked') != 'checked'){
		   required_past = false;
			$(this).parent().addClass('required_error');
		 }
	    else if($(this).val() == '' || $(this).val() == 0){
		   required_past = false;
			$(this).addClass('required_error');
		 }
	  });
	  /* jeżeli nie są wypełnione, wyświetlamy alert */
	  if(required_past == false){
		 show_error_box(form_id);
		 $('#'+form_id+' .error_box').html('prosimy wypełnić wymagane pola');
	  }
	  error_fields_ready();
	  return required_past;
	});

  $('.change_search_method').click(function(){
    $('.change_search_method').parent().removeClass('active');
	 $(this).parent().addClass('active');
	 $('#search_method').val($(this).parent().index());
    return false;
  });
}

/*
 * inne akcje dla form
*/
function ohter_form_action_ready(){
  $('.check_parent').bind('keyup keydown change', function(){
    var id = $(this).attr('id').replace('_ainput','');
	 $('#'+id).attr('checked','checked');
  });
}

/* usuwamy error gdy wypełnione */
function error_fields_ready(){
   $('.required_error').unbind('change keydown');
	$('.required_error').bind('change keydown',function(){
	  if($(this).val() != ''){
	    $(this).removeClass('required_error');
	  }
	});
	$('.required_field').unbind('click');
	$('.required_field').bind('change',function(){
     if($(this).attr('type') == 'radio' && $(this).attr('checked') == 'checked'){
	    var main = $(this).parent();
	    $(main).removeClass('required_error');
		 $(main).children('ul').children('li').children('input[type=radio]').removeClass('required_field');
	  }
     else if($(this).attr('type') == 'checkbox' && $(this).attr('checked') == 'checked'){
	    var main = $(this).parent();
	    $(main).removeClass('required_error');
		 $(main).children('ul').children('li').children('input[type=checkbox]').removeClass('required_field');
	  }
	});
}

/* sprawdzamy wszystkie pola */
function check_all_fields(){
  $('input').each(function(){
	 if(($(this).attr('type') == 'radio' || $(this).attr('type') == 'checkbox') && $(this).attr('checked') == 'checked'){
	   $(this).removeClass('required_field');
		$(this).parent().parent().children('li').children('input').removeClass('required_field');
	 }
  });
}

function show_error_box(form_id){
  if($('#'+form_id+' .error_box').length == 0){
    var html = '<div class="error_box alert alert-error" style="position:absolute;z-index:2;top:-40px;left:0px">prosimy wypełnić wymagane pola</div>';
	 $('#'+form_id+' button').last().parent().css({'position':'relative'});
	 $('#'+form_id+' button').last().parent().append(html);
	 setTimeout("$('#"+form_id+" .error_box').fadeOut('slow')",3000);
  }
  else{
    $('#'+form_id+' .error_box').show();
	 setTimeout("$('#"+form_id+" .error_box').fadeOut('slow')",3000);
  }
}


/*
 * akcje dla wstawiania linku
 */
function insert_link_ready(field_id){
  $('.form_url').focus(function(){
    var id = $(this).attr('id');
	 $('#url_box_'+id).show();
	 $('#url_box_'+id+' .url_address').val($(this).val().replace('http://',''));
	 $('#url_box_'+id+' .url_address').focus();
  });
  $('.close_url_box').click(function(){
    $('#url_box_'+field_id).hide();
  });
  $('#url_box_'+field_id+' button').click(function(){
    var url_error = false;
	 var id = $(this).parent().attr('id');
    var url_protocol = $('#url_box_'+field_id+' .url_type').val();
	 var url_address = $('#url_box_'+field_id+' .url_address').val().replace(url_protocol,'');
	 if((url_protocol == 'http://' || url_protocol == 'ftp://') && is_valid_URL(url_address) == false){
	   url_error = 'nieprawidłowy adres url';
	 }
	 else if(url_protocol == 'mailto:' && is_valid_email(url_address) == false){
	   url_error = 'nieprawidłowy email';
	 }

	 if(url_error == false || url_address == ''){
	   $('#url_box_'+field_id+' .url_error').html('');
		if(url_protocol == 'http://' || url_protocol == 'ftp://'){
	     $('#'+field_id).val(url_protocol+url_address);
		}
		else{
		  $('#'+field_id).val(url_address);
		}
	   $('#url_box_'+field_id).hide();
	 }
	 else{
	   $('#url_box_'+field_id+' .url_error').html(url_error);
	 }
	 return false;
  });
}

  function is_valid_URL(url){
    var RegExp = /^(([\w]+:)?\/\/)?(([\d\w]|%[a-fA-f\d]{2,2})+(:([\d\w]|%[a-fA-f\d]{2,2})+)?@)?([\d\w][-\d\w]{0,253}[\d\w]\.)+[\w]{2,4}(:[\d]+)?(\/([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)*(\?(&?([-+_~.\d\w]|%[a-fA-f\d]{2,2})=?)*)?(#([-+_~.\d\w]|%[a-fA-f\d]{2,2})*)?$/;
    if(RegExp.test(url)){
      return true;
    }
    else{
      return false;
    }
  }

  function is_valid_email(email){
    var RegExp = /^((([a-z]|[0-9]|!|#|$|%|&|'|\*|\+|\-|\/|=|\?|\^|_|`|\{|\||\}|~)+(\.([a-z]|[0-9]|!|#|$|%|&|'|\*|\+|\-|\/|=|\?|\^|_|`|\{|\||\}|~)+)*)@((((([a-z]|[0-9])([a-z]|[0-9]|\-){0,61}([a-z]|[0-9])\.))*([a-z]|[0-9])([a-z]|[0-9]|\-){0,61}([a-z]|[0-9])\.)[\w]{2,4}|(((([0-9]){1,3}\.){3}([0-9]){1,3}))|(\[((([0-9]){1,3}\.){3}([0-9]){1,3})\])))$/
    if(RegExp.test(email)){
        return true;
    }else{
        return false;
    }
  }

/*
 * akcje dla ograniczonego pola
*/
function restricted_ready(field_id, allowed_chars){
  $('#'+field_id).bind('keypress',function(e){
	 var key = e.charCode || e.keyCode || 0;
	 //console.log(key);
	 if(key == 8 || key == 46 || key == 9 || (key > 36 && key < 41)){
	   return true;
	 }
	 else if(key > 47 && key < 58 && $(this).hasClass('allow_int')){
	   return true;
	 }
	 else if(key > 96 && key < 123 && $(this).hasClass('allow_alpha')){
	   return true;
	 }
	 else{
	   return false;
	 }
  });
}

/*
 * akcję dla dialogu selecta
*/
function dialog_select_ready(field_id,element){
  $('#'+field_id).click(function(){
    $('#'+field_id+'_dialog').dialog({
	   height: element.height,
		width: element.width,
		title: 'wybierz z listy',
		modal: true
	 });
	 dialog_buttons_ready(field_id);
  });
}

function dialog_buttons_ready(field_id){
  $('.nolink').click(function(){
	 //$('#'+field_id).append('<option value="'+$(this).attr('id').replace('element_','')+'">'+$(this).html()+'</option>');
	 $('#'+field_id).val($(this).html().replace(/(<([^>]+)>)/ig,""));
	 $('#'+field_id+'_value').val($(this).attr('id').replace('element_',''));
	 $('#'+field_id+'_dialog').dialog('close');
	 $(this).parent().parent().children('li').removeClass('active');
	 $(this).parent().addClass('active');
	 return false;
  });
}