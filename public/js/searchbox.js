/**
 * @framework 3wymiar.pl 3.0 2002-2013 (c)
 *
 * @author Marcin Cichy <cichy@3wymiar.pl>
 * @version 1.0
 */

$(document).ready(function(){
  searchbox_ready();
});

function searchbox_ready(){
  $('#category_list').disableSelection();

  $('body').click(function(){
    if($('#category_list').css('display') != 'none'){
	   $('#category_list').slideUp(200);
	 }
	 if($('#search_list').css('display') != 'none'){
	   $('#search_list').slideUp(200);
	 }
  });

  $('.showhide_category').click(function(event){
    event.stopPropagation();
    if($('#category_list').css('display') == 'none'){
      $('#category_list').slideDown(500);
	//	$('.icon_category').attr('src',$('.icon_category').attr('src').replace('down','up'));
	 }
	 else{
	   $('#category_list').slideUp(200);
//		$('.icon_category').attr('src',$('.icon_category').attr('src').replace('up','down'));
	 }
  });
  $('.cat_checkbox').change(function(){
    after_change_category();
  });
  $('#category_list').click(function(event){
    if($(event.target).hasClass('cat_checkbox') == false){
      return false;
	 }
	 else{
	   event.stopPropagation();
	 }
  });
  $('#category_list li').click(function(event){
    if($(event.target).hasClass('cat_checkbox') == true){}
    else if($(this).children('input').attr('checked') == 'checked'){
	   $(this).children('input').removeAttr('checked');
	 }
	 else{
	   $(this).children('input').attr('checked','checked');
	 }

    after_change_category();
  });
  $('.search_input').val('');
  $('.search_input').click(function(event){
    if($(this).val() != ''){
	   $('#search_list').slideDown(500);
		event.stopPropagation();
	 }
  });
  $('.search_input').keyup(function(e){
    var key = e.charCode || e.keyCode || 0;
	 if((key > 47 && key < 91) || key == 8 || key == 46){
	   searchNow();
	 }
	 else if(key == 37){ // lewa
		$('#search_list li.active').removeClass('active');
	   $('#search_list li').first().addClass('active');
	 }
	 else if(key == 39){ // prawa
		$('#search_list li.active').removeClass('active');
		$('#search_list li').last().addClass('active');
	 }
	 else if(key == 38){ // gora
	   if($('#search_list li.active').length == 0){
		  $('#search_list li').last().addClass('active');
		}
		else{
		  if($('#search_list li.active').is(':first-child') == true){
		    $('#search_list li.active').removeClass('active');
		    $('#search_list li').last().addClass('active');
		  }
		  else{
		    var el = $('#search_list li.active').prev();
		    $('#search_list li.active').removeClass('active');
			 $(el).addClass('active');
		  }
		}
	 }
	 else if(key == 40){ // dol
	   if($('#search_list li.active').length == 0){
		  $('#search_list li').first().addClass('active');
		}
		else{
		  if($('#search_list li.active').is(':last-child') == true){
		    $('#search_list li.active').removeClass('active');
		    $('#search_list li').first().addClass('active');
		  }
		  else{
		    var el = $('#search_list li.active').next();
		    $('#search_list li.active').removeClass('active');
			 $(el).addClass('active');
		  }
		}
	 }
	 else if(key == 13){ // enter
	   if($('#search_list li.active').length == 1){
	     document.location.href = $('#search_list li.active').children('a').attr('href');
		}
	 }
  });
}

function search_mouseover(){
  $('#search_list li').unbind('click mouseover mouseout');
  $('#search_list li').click(function(){
    document.location.href = $(this).children('a').attr('href');
  });
  $('#search_list li').mouseover(function(){
    $('#search_list li.active').removeClass('active');
	 $(this).addClass('active');
  });
  $('#search_list li').mouseout(function(){
	 $(this).removeClass('active');
  });
}

function after_change_category(){
  searchNow();
  if($('.cat_checkbox:checked').length == $('.cat_checkbox').length){
	 $('.category_input').html('wszystkie');
  }
  else{
	 if($('.cat_checkbox:checked').length > 4){
	   $('.category_input').html('wybrano '+$('.cat_checkbox:checked').length+' kategorii');
	 }
	 else if($('.cat_checkbox:checked').length > 1){
	   $('.category_input').html('wybrano '+$('.cat_checkbox:checked').length+' kategorie');
	 }
	 else if($('.cat_checkbox:checked').length == 1){
	   $('.category_input').html('wybrano '+$('.cat_checkbox:checked').length+' kategorię');
	 }
	 else{
	   $('.category_input').html('nie wybrano kategorii');
	 }
  }
}

function searchNow(){
  if($('.search_input').val() != '' && $('.cat_checkbox:checked').length > 0){
 //   $('.icon_search').attr('src',$('.icon_search').attr('src').replace('layout/search.png','ajax/loader1.gif'));
	 $('#search_list').show();

	 var cat = '';
	 $('.cat_checkbox:checked').each(function(){
	   cat += $(this).val()+',';
	 });
	 cat = cat.substr(0,(cat.length-1));

	 $.ajax({
      url: 'ajax.php?module=search&q='+$('.search_input').val()+'&c='+cat,
	   type: 'GET',
      success: function(odp){
	//	  $('.icon_search').attr('src',$('.icon_search').attr('src').replace('ajax/loader1.gif','layout/search.png'));
	     $('#search_list').html(odp);
		  search_mouseover();
      }
    });
  }
  else{
	 $('#search_list').hide();
  }
}