/*!
  * jHtmlArea
  *
  * @author: http://jhtmlarea.codeplex.com - (c)2009 Chris Pietschmann
  * @version 0.7.0
  *
*/
var editor;

$(function () {
    if ($(".dialogEditor").length > 0) {
        $(".dialogEditor").htmlarea({
            toolbar: [["html"], ["bold", "italic", "underline"], ["justifyLeft", "justifyCenter", "justifyRight"], ["orderedList", "unorderedList"], ["p", "h3", "h2", "h1", "|", "forecolor", "|", "superscript", "subscript"], ["link", "unlink"], [
                {
                    css: 'img',
                    text: 'wstaw obrazek z galerii',
                    action: function (btn) {
                        insert_into_editor(btn);
                    }
                },
                {
                    css: 'movie',
                    text: 'wstaw film z YouTube',
                    action: function (btn) {
                        insert_into_editor(btn);
                    }
                },
                {
                    css: 'file',
                    text: 'wstaw plik do pobrania',
                    action: function (btn) {
                        insert_into_editor(btn);
                    }
                }
            ]],
            css: "../public/css/bootstrap/bootstrap-editor.min.css"
        });
        $(body).append('<div id="editor_dialog" style="display:none"><div class="editor_ajax_loader"></div></div>');
    }
});

/*!
  * dodatkoew funckje edytora
  * @framework 3wymiar.pl 3.0 2002-2012 (c)
  *
  * @author Marcin Cichy <cichy@3wymiar.pl>
  * @version 1.0
*/
$(document).ready(function () {
    editor_functions();
});

function editor_functions() {
    if ($('.dialogEditor').length > 0) {
        $('.dialogEditor').parent().addClass('border_round');
    }
}


function insert_into_editor(btn) {
    $('#editor_dialog').dialog({
        dialogClass: 'fixed_position',
        width: 1200,
        height: ($(window).height() - 100),
        modal: true,
        title: $(btn).attr('title')
    });

    $.ajax({
        url: 'ajax.php?module=dialog_editor&action=' + $(btn).attr('class'),
        type: 'GET',
        success: function (ans) {
            $('#editor_dialog').html(ans);
            if ($(btn).attr('class') == 'img') {
                upload_img_ready();
                insert_items_ready();
                delete_items_ready();
            } else if ($(btn).attr('class') == 'movie') {
                upload_movie_ready();
            } else if ($(btn).attr('class') == 'file') {
                upload_file_ready();
                insert_items_ready();
                delete_items_ready();
            }
            editor_nav_ready();
        }
    });
}

function editor_nav_ready() {
    $('.dialog_editor_nav li a').click(function () {
        insert_into_editor($(this));
        return false;
    });
}

function upload_img_ready() {
    $('.upload_maker').change(function () {
        if ($(this).attr('checked') == 'checked') {
            $(this).parent('.upload_box').children('.upload_option_box').slideDown('slow');
        } else {
            $(this).parent('.upload_box').children('.upload_option_box').slideUp('slow');
        }
    });
    $('.upload_type').on('change', function () {
        var selected = $('input[name=' + $(this).attr('name') + ']:checked').attr('id').split('_');
        if (selected[2] == 1) {
            $('input[name=' + selected[0] + '_size]').attr('placeholder', 100);
            $('input[name=' + selected[0] + '_width]').removeAttr('placeholder');
            $('input[name=' + selected[0] + '_height]').removeAttr('placeholder');
            $('input[name=' + selected[0] + '_width]').val('');
            $('input[name=' + selected[0] + '_height]').val('');
        } else if (selected[2] == 2) {
            $('input[name=' + selected[0] + '_size]').removeAttr('placeholder');
            $('input[name=' + selected[0] + '_size]').val('');
            $('input[name=' + selected[0] + '_width]').attr('placeholder', 640);
            $('input[name=' + selected[0] + '_height]').attr('placeholder', 480);
        }
    });
    $('.inp_int').bind('keypress', function (e) {
        var key = e.charCode || e.keyCode || 0;
        if (key == 8 || key == 46 || key == 9 || (key > 36 && key < 41)) {
            return true;
        } else if (key > 47 && key < 58) {
            var selected = $(this).attr('name').split('_');
            if (selected[1] == 'size') {
                $('#' + selected[0] + '_type_1').attr('checked', 'checked').trigger('change');
            } else {
                $('#' + selected[0] + '_type_2').attr('checked', 'checked').trigger('change');
            }
            return true;
        } else {
            return false;
        }
    });

    $("#begin_upload_img").click(function () {
        var thumbnail_json = '[0]';
        var scaled_json = '[0]';

        if ($('#make_thumbnail').attr('checked') == 'checked') {
            thumbnail_json = '[1,' + $('input[name=thumbnail_type]:checked').val();
            if ($('input[name=thumbnail_type]:checked').val() == 1) {
                var size = ($('input[name=thumbnail_size]').val() == '') ? '"default"' : parseInt($('input[name=thumbnail_size]').val());
                thumbnail_json += ',' + size;
            } else if ($('input[name=thumbnail_type]:checked').val() == 2) {
                var width = ($('input[name=thumbnail_width]').val() == '') ? '"default"' : parseInt($('input[name=thumbnail_width]').val());
                var height = ($('input[name=thumbnail_height]').val() == '') ? '"default"' : parseInt($('input[name=thumbnail_height]').val());
                thumbnail_json += ',' + width + ',' + height;
            }
            thumbnail_json += ']';
        }

        if ($('#make_scaled').attr('checked') == 'checked') {
            scaled_json = '[1,' + $('input[name=scaled_type]:checked').val();
            if ($('input[name=scaled_type]:checked').val() == 1) {
                var size = ($('input[name=scaled_size]').val() == '') ? '"default"' : parseInt($('input[name=scaled_size]').val());
                scaled_json += ',' + size;
            } else if ($('input[name=scaled_type]:checked').val() == 2) {
                var width = ($('input[name=scaled_width]').val() == '') ? '"default"' : parseInt($('input[name=scaled_width]').val());
                var height = ($('input[name=scaled_height]').val() == '') ? '"default"' : parseInt($('input[name=scaled_height]').val());
                scaled_json += ',' + width + ',' + height;
            }
            scaled_json += ']';
        }

        if ($("#upload_img").val() == '') {
            return false;
        }

        var form_data = new FormData();
        form_data.append('file', document.getElementById('upload_img').files[0]);
        form_data.append('thumbnail', thumbnail_json);
        form_data.append('scaled', scaled_json);
        form_data.append('action', 'img');

        $.ajax({
            type: 'POST',
            url: 'ajax.php?module=dialog_editor&action=img_upload',
            xhr: function () {
                myXhr = $.ajaxSettings.xhr();
                if (myXhr.upload) {
                    myXhr.upload.addEventListener('progress', progressHandlingFunction, false);
                }
                return myXhr;
            },
            beforeSend: function (e) {
                $('#ajax_upload_dialog').dialog({
                    modal: true,
                    width: 400,
                    height: 150,
                    title: 'wgrywanie pliku',
                    dialogClass: 'fixed_position',
                    resizable: false,
                    draggable: false,
                    open: function (event, ui) {
                        $(this).parent().children().children('.ui-dialog-titlebar-close').hide();
                    }
                });
            },
            success: function (e) {
                $('#ajax_upload_dialog').dialog('destroy');
                $('#upload_img').val('');
                $.ajax({
                    url: 'ajax.php?module=dialog_editor&action=clear_img',
                    type: 'GET',
                    success: function (ans) {
                        $('#editor_content').html(ans);
                        insert_items_ready();
                        delete_items_ready();
                    }
                });
            },
            error: function (e) {
            },
            data: form_data,
            cache: false,
            contentType: false,
            processData: false
        });
    });
}

function progressHandlingFunction(e) {
    if (e.lengthComputable) {
        var percent = parseInt(((e.loaded / e.total) * 100), 0);
        $('#upload_progress').css({width: percent + '%'});
    }
}

function upload_movie_ready() {
    var movie_html;
    $('#begin_upload_movie').click(function () {
        if ($('#upload_movie').val() == '') {
            $('#upload_movie').addClass('required_error');
            return false;
        }
        var width = ($('#movie_width').val() != '') ? $('#movie_width').val() : 600;
        var height = ($('#movie_height').val() != '') ? $('#movie_height').val() : 400;
        var movie = $('#upload_movie').val();
        movie_html = '<iframe width="' + width + '" height="' + height + '" src="http://www.youtube.com/embed/' + movie + '" frameborder="0" allowfullscreen></iframe>';
        var html = '<div class="alert alert-success">Poniżej znajduje się podgląd filmu.<p>Jeżeli wszystko jest zgodnie z oczekiwaniami kliknij <span class="btn btn-success" id="insert_movie"><i class="icon-plus icon-white"></i><i class="icon-film icon-white"></i> wstaw film</span></p></div>' + movie_html;
        $('#editor_content').html(html);
        insert_movie_ready();
    });

    function insert_movie_ready() {
        $('#insert_movie').unbind('click');
        $('#insert_movie').click(function () {
            $(".dialogEditor").htmlarea('pasteHTML', movie_html);
            $('#editor_dialog').dialog('destroy');
        });
    }

    $('#upload_movie').keyup(function () {
        if ($(this).val() != '') {
            $(this).removeClass('required_error');
        }
    });
}

function upload_file_ready() {
    $("#begin_upload_file").click(function () {
        var form_data = new FormData();
        form_data.append('file', document.getElementById('upload_file').files[0]);
        form_data.append('action', 'file');

        $.ajax({
            type: 'POST',
            url: 'ajax.php?module=dialog_editor&action=file_upload',
            xhr: function () {
                myXhr = $.ajaxSettings.xhr();
                if (myXhr.upload) {
                    myXhr.upload.addEventListener('progress', progressHandlingFunction, false);
                }
                return myXhr;
            },
            beforeSend: function (e) {
                $('#ajax_upload_dialog').dialog({
                    modal: true,
                    width: 400,
                    height: 150,
                    title: 'wgrywanie pliku',
                    dialogClass: 'fixed_position',
                    resizable: false,
                    draggable: false,
                    open: function (event, ui) {
                        $(this).parent().children().children('.ui-dialog-titlebar-close').hide();
                    }
                });
            },
            success: function (e) {
                $('#ajax_upload_dialog').dialog('destroy');
                $('#upload_file').val('');
                $.ajax({
                    url: 'ajax.php?module=dialog_editor&action=clear_file',
                    type: 'GET',
                    success: function (ans) {
                        $('#editor_content').html(ans);
                        insert_items_ready();
                        delete_items_ready();
                    }
                });
            },
            error: function (e) {
            },
            data: form_data,
            cache: false,
            contentType: false,
            processData: false
        });
    });
}

function insert_items_ready() {
    $('.insertThis').unbind('click');
    $('.insertThis').click(function () {
        var insert = new Object();
        insert.file = $(this).attr('insert_file');
        insert.type = $(this).attr('insert_type');
        insert.class = '';
        insert.html = '';

        if ($('#add_polaroid').attr('checked') == 'checked') {
            insert.class += 'img-polaroid';
        }

        if (insert.type == 'img') {
            insert.html = '<img src="' + insert.file + '" class="' + insert.class + '" alt="" />';
        } else if (insert.type == 'thumbnail') {
            insert.html = '<img src="' + insert.file + '" class="magic_thumbnail ' + insert.class + '" alt="" />';
        } else if (insert.type == 'download') {
            insert.html = '<a href="' + insert.file + '" class="btn"><i class="icon-download">&nbsp;</i> pobierz</a>';
        }
        $(".dialogEditor").htmlarea('pasteHTML', insert.html);
        $('#editor_dialog').dialog('destroy');
    });
}

function delete_items_ready() {
    $('.deleteFromEditor').unbind('click');
    $('.deleteFromEditor').click(function () {
        if (!confirm('Czy jesteś pewny(a), że chcesz skasować ' + $(this).attr('title').replace('skasuj', '') + '?\nPrzed skasowaniem upewnij się, że żaden komponent nie wyświetla tego pliku.\nTen proces jest nieodwracalny.')) {
            return false;
        }
        var attrFile = $(this).attr('delete_file').split('|');
        var delete_file = new Object();
        delete_file.file = attrFile[0];
        delete_file.type = attrFile[1];
        delete_file.ext = attrFile[2];

        $.ajax({
            url: 'ajax.php?module=dialog_editor&action=delete&file=' + delete_file.file + '&ext=' + delete_file.ext + '&type=' + delete_file.type,
            type: 'GET',
            success: function (ans) {
                $.ajax({
                    url: 'ajax.php?module=dialog_editor&action=clear_' + delete_file.type,
                    type: 'GET',
                    success: function (ans) {
                        $('#editor_content').html(ans);
                        insert_items_ready();
                        delete_items_ready();
                    }
                });
            }
        });
    });
}