/**
 * @framework 3wymiar.pl 3.0 2002-2013 (c)
 *
 * @author Marcin Cichy <cichy@3wymiar.pl>
 * @version 1.0
 */

/* wyłaczenie firebuga */
if (!console) {
 var con = function () {
  this.log = function (str) {
  }
 }
 var console = new con();
}

/* start serwisu */
$(document).ready(function(){
 
  delete_this_elements();
  make_magic_thumbnails();
});

$(window).resize(function(){
  
});

/* kasowanie obiektów */
function delete_this_elements(){
  $('.deleteThis').unbind('click');
  $('.deleteThis').click(function(){
    if (!confirm('Czy jesteś pewny(a), że chcesz skasować '+$(this).attr('title').replace('skasuj','')+'?\nTen proces jest nieodwracalny.')){
	   return false;
	 }
  });
}

/* ustawianie strony */
function calibrate_site(){
  var nheight = $(window).height()-$('#site_header_box').height()-$('#site_footer_box').height()-150;
  $('#site_mainframe').css({height:'auto'});
  if($('#site_mainframe').height() < nheight){
    $('#site_mainframe').css({height:nheight+'px'});
  }
}

/* formatowanie miniaturek do modalnych okien*/
function make_magic_thumbnails(){
  $('.magic_thumbnail').each(function(){
	 var insert = new Object();
	 insert.file = $(this).attr('src');
	 insert.uniqid = uniqid();
	 $(this).wrap('<a href="#'+insert.uniqid+'" class="magic_corner" data-toggle="modal" />');
	 $(this).parent().parent().append('<div id="'+insert.uniqid+'" class="modal hide fade" role="dialog" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&#215;</button><h3>&nbsp;</h3></div><div class="modal-body"><img src="'+insert.file.replace('_thumb','')+'" class="'+insert.class+'" alt="" /></div><div class="modal-footer"><span class="btn" data-dismiss="modal" aria-hidden="true">Zamknij</span></div></div>');
  });
}

function uniqid(){
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for( var i=0; i < 5; i++ ){
      text += possible.charAt(Math.floor(Math.random() * possible.length));
	 }
    return text;
}

/* obsługa ciasteczek */
function setCookie(c_name,value,exdays){
  var exdate=new Date();
  exdate.setDate(exdate.getDate() + exdays);
  var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
  document.cookie=c_name + "=" + c_value;
}

function getCookie(c_name){
  var i,x,y,ARRcookies=document.cookie.split(";");
  for (i=0;i<ARRcookies.length;i++){
    x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
    y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
    x=x.replace(/^\s+|\s+$/g,"");
    if (x==c_name){
      return unescape(y);
    }
  }
}

/* dodawanie i wywalanie slashy */
function addslashes(str) {
  str=str.replace(/\\/g,'\\\\');
  str=str.replace(/\'/g,'\\\'');
  str=str.replace(/\"/g,'\\"');
  str=str.replace(/\0/g,'\\0');
  return str;
}
function stripslashes(str) {
  str=str.replace(/\\'/g,'\'');
  str=str.replace(/\\"/g,'"');
  str=str.replace(/\\0/g,'\0');
  str=str.replace(/\\\\/g,'\\');
  return str;
}

/* validacja email */
function checkEmail(email) {
  var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  if (!filter.test(email)) {
    return false;
  }
}

/* tworzenie $_GET jak w PHP */
var parts = window.location.search.substr(1).split("&");
var $_GET = {};
for (var i = 0; i < parts.length; i++) {
    var temp = parts[i].split("=");
    $_GET[decodeURIComponent(temp[0])] = decodeURIComponent(temp[1]);
}