/**
 * @framework 3wymiar.pl 3.0 2002-2013 (c)
 *
 * @author Marcin Cichy <cichy@3wymiar.pl>
 * @version 1.0
 */

$(document).ready(function(){
  demo_ready();
});

function demo_ready(){
  $('#dialog').dialog({
    modal: true,
	 width:400,
	 resizable: false,
	 draggable: false,
    closeOnEscape: false,
	 title: 'strona w budowie',
    open: function(event, ui) {
      $(this).parent().children().children('.ui-dialog-titlebar-close').hide();
    }
  });
}